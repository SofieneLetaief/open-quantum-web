<?php

namespace VenteBundle\Controller;

use OpenQuantumBundle\Entity\Lignestock;
use OpenQuantumBundle\Entity\Promotion;
use OpenQuantumBundle\Entity\Societe;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use OpenQuantumBundle\Entity\Article;
use VenteBundle\Entity\Commande;
use VenteBundle\Entity\LigneCommande;
use VenteBundle\Entity\Livreur;


class DefaultController extends Controller
{


    public function indexAction(SessionInterface $session)
    {
        return $this->render('@Vente/Default/index.html.twig');
    }


    public function addProduitAction(SessionInterface $session,$idp, $qte){

        $session->start();
        if ($session->get('panier') == null)
            $session->set('panier',[]);
        $panier = $session->get('panier');
        if(isset($panier[$idp])){

                $panier[$idp] += $qte;
        }
        else {
            $panier[$idp] = $qte;
        }
        $session->set('panier',$panier);
        return new JsonResponse(sizeof($panier));
    }


    public function PanierAction(SessionInterface $session,Request $request){
        $session->start();
        if ($session->get('panier') == null)
            $session->set('panier',[]);
        $em = $this->getDoctrine()->getManager();
        $panier = $session->get('panier');
        $articles = [];
        foreach ($panier as $p=>$qte){
          
            $articles[$p] = $em->getRepository(Article::class)->find($p);
        }

        //promotion mannai
        $coupon = $request->query->get('couponcode');
        $promotion = $em->getRepository(Promotion::class)->findOneBy(['code' => $coupon]);
        if(is_null($promotion)) {
            $pourcentage = 0;
        }
        else{
            $pourcentage = $promotion->getPourcentage();
        }

        dump($promotion);
        return $this->render('panier.html.twig', ["panier"=>$panier, "articles"=>$articles,"pourcentage"=>$pourcentage]);
    }


    public function setArticleAction(SessionInterface $session,$idp, $qte){

        $em = $this->getDoctrine()->getManager();
        $session->start();
        if ($session->get('panier') == null)
            $session->set('panier',[]);
        $panier = $session->get('panier');

        $panier[$idp] = $qte;

        $session->set('panier',$panier);
        $total = 0;
        foreach ($panier as $k=>$v){
            $total += $v * $em->getRepository(Article::class)->find($k)->getPrixvente();
        }
        return new JsonResponse($total);
    }


    public function CheckOutAction(SessionInterface $session){

        $em = $this->getDoctrine()->getManager();
        $session->start();
        if ($session->get('panier') == null)
            $session->set('panier',[]);

        $user =   $this->container->get('security.token_storage')->getToken()->getUser();
        $societeId = $user->getIdSociete();
        $panier = $session->get('panier');
        $commande = new Commande();
        $livreur  = $em->getRepository(Livreur::class)->find(1);
        $commande->setLivreur($livreur);
        $commande->setDate(new \DateTime());
        $commande->setEtatLivraison('En attente');
        $commande->setUtilisateur($this->getUser());
        $commande->setActive(true);
        $commande->setSociete($societeId);

        foreach ($panier as $k=>$v){
            $art = $em->getRepository(Article::class)->find($k);

           $lignestock  = $em->getRepository(Lignestock::class)->findOneBy(['idarticle' => $k] );

            $ligne = new LigneCommande();
            $ligne->setCommande($commande);
            $res=$lignestock->getQteStock()-$v;
            $lignestock->setQteStock($res);
            $ligne->setActive(true);
            $ligne->setArticle($art);
            $ligne->setQuantite($v);
            $em->persist($ligne);
            //$this->getDoctrine()->getManager()->flush();
            $commande->addLigne($ligne);

        }


        /*
        //returns an instance of Vresh\TwilioBundle\Service\TwilioWrapper
        $twilio = $this->get('twilio.api');
        $username=$user->getNom();
        $message = $twilio->account->messages->sendMessage(
            '+17069141968', // From a Twilio number in your account
            '+21699377944', // Text any number
            " bonjour " .$username. " votre commande a été effctué vous serez notifié lorsque votre commande est envoyé !"
        );
        */

        $em->persist($commande);
        $em->flush();
        $session->clear();

        return $this->redirectToRoute('commande_index_front');

      //  return new Response($message->sid);
    }

    public function removeAction($id , SessionInterface $session){
        $panier= $session->get('panier',[]);
        if(!empty($panier[$id])){
            unset($panier[$id]);
        }
        $session->set('panier', $panier);
        return $this->redirectToRoute("card");

    }












}
