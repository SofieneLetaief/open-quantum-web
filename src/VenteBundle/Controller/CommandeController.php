<?php

namespace VenteBundle\Controller;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use VenteBundle\Entity\Commande;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use VenteBundle\Entity\Livreur;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Knp\Bundle\SnappyBundle\Snappy\Response\JpegResponse;


/**
 * Commande controller.
 *
 */
class CommandeController extends Controller
{
    /**
     * Lists all commande entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $idSociete = $user->getIdSociete();
        $commandes = $em->getRepository('VenteBundle:Commande')->findBy(['societe'=>$idSociete,'active'=>1]);

        
        $em->flush();

        return $this->render('commande/index.html.twig', array(
            'commandes' => $commandes,
        ));
    }

    /**
     * Finds and displays a commande entity.
     *
     */
    public function showAction(Request $request,Commande $commande)
    {

        $form = $this->createFormBuilder()
            ->add('Livreur', EntityType::class,['class'=>Livreur::class])
            ->add('send', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // data is an array with "name", "email", and "message" keys
            $data = $form->getData();
            $commande->setLivreur($data['Livreur']);
        }
        $this->getDoctrine()->getManager()->flush();
        return $this->render('commande/show.html.twig', array(
            'commande' => $commande,
            'form'=>$form->createView()
        ));
    }

    /**
     * Deletes a review entity.
     *
     */
    public function deleteAction(Request $request, Commande $commande)
    {
        $form = $this->createDeleteForm($commande);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $commande->setActive(false);
            $em->flush();
        }

        return $this->redirectToRoute('commande_index');
    }


    public function indexFrontAction()
    {
        $em = $this->getDoctrine()->getManager();

        $commandes = $em->getRepository('VenteBundle:Commande')->findBy(['utilisateur'=>$this->getUser()]);
        $em->flush();

        return $this->render('@Vente/commandes.html.twig', array(
            'commandes' => $commandes,
        ));
    }

    private function createDeleteForm(Commande $commande)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('commande_delete', array('id' => $commande->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }

    public function printAction (Commande $commande,Request $request){

        $deleteForm = $this->createDeleteForm($commande);
        $snappy = $this->get('knp_snappy.pdf');

        $html = $this->renderView('commande/pdf.html.twig', array(
            'commande' => $commande,
            'delete_form' => $deleteForm->createView(),
        ));

        $filename = 'myFirstSnappyPDF';



        return new Response(
            $snappy->getOutputFromHtml($html),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'inline; filename="'.$filename.'.pdf"'
            )
        );
    }

    /*public function imageAction()
    {
        $html = $this->renderView('MyBundle:Foo:bar.html.twig', array(
            'some'  => $vars
        ));

        return new JpegResponse(
            $this->get('knp_snappy.image')->getOutputFromHtml($html),
            'image.jpg'
        );
    }*/


}
