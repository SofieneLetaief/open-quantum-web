<?php

namespace VenteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Lignecommande
 *
 * @ORM\Table(name="lignecommande", indexes={@ORM\Index(name="idArticle", columns={"idArticle"}), @ORM\Index(name="idCommande", columns={"idCommande"})})
 * @ORM\Entity
 */
class Lignecommande
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idLigneCommande", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idlignecommande;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantite", type="integer", nullable=false)
     */
    private $quantite;

    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="integer", nullable=false)
     */
    private $active;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="\OpenQuantumBundle\Entity\Article")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idArticle", referencedColumnName="idArticle")
     * })
     */
    private $article;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="Commande")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idCommande", referencedColumnName="idCommande")
     * })
     */
    private $commande;

    /**
     * @return mixed
     */
    public function getArticle()
    {
        return $this->article;
    }



    /**
     * @param mixed $article
     */
    public function setArticle($article)
    {
        $this->article = $article;
    }



    /**
     * @return int
     */
    public function getIdlignecommande()
    {
        return $this->idlignecommande;
    }

    /**
     * @param int $idlignecommande
     */
    public function setIdlignecommande($idlignecommande)
    {
        $this->idlignecommande = $idlignecommande;
    }

    /**
     * @return int
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * @param int $quantite
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;
    }

    /**
     * @return int
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param int $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return \Commande
     */
    public function getIdcommande()
    {
        return $this->commande;
    }

    /**
     * @param \Commande $idcommande
     */
    public function setIdcommande($idcommande)
    {
        $this->idcommande = $idcommande;
    }

    /**
     * Set commande
     *
     * @param \VenteBundle\Entity\Commande $commande
     *
     * @return LigneCommande
     */
    public function setCommande(\VenteBundle\Entity\Commande $commande = null)
    {
        $this->commande = $commande;

        return $this;
    }

    /**
     * Get commande
     *
     * @return \VenteBundle\Entity\Commande
     */
    public function getCommande()
    {
        return $this->commande;
    }








}

