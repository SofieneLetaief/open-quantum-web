<?php

namespace VenteBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * Commande
 *
 * @ORM\Table(name="commande", indexes={@ORM\Index(name="idUtilisateur", columns={"idUtilisateur"}), @ORM\Index(name="idSociete", columns={"idSociete"}), @ORM\Index(name="idLivreur", columns={"idLivreur"})})
 * @ORM\Entity
 */
class Commande
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idCommande", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="etatLivraison", type="string" , nullable=false)
     */

    private $etatLivraison;

    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="integer", nullable=false)
     */
    private $active;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="OpenQuantumBundle\Entity\Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idUtilisateur", referencedColumnName="idUtilisateur")
     * })
     */
    private $utilisateur;

    /**
     *
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="OpenQuantumBundle\Entity\Societe")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idSociete", referencedColumnName="idSociete")
     * })
     */
    private $societe;

    /**
     * @return int
     */
    public function getSociete()
    {
        return $this->societe;
    }

    /**
     * @param int $societe
     */
    public function setSociete($societe)
    {
        $this->societe = $societe;
    }





    /**
     * @var \Livreur
     *
     * @ORM\ManyToOne(targetEntity="Livreur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idLivreur", referencedColumnName="idLivreur")
     * })
     */
    private $livreur;

    /**
     * @ORM\OneToMany(targetEntity="VenteBundle\Entity\LigneCommande",mappedBy="commande")
     */
    private $lignes;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->lignes = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * @return int
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param int $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return \Livreur
     */
    public function getLivreur()
    {
        return $this->livreur;
    }

    /**
     * @param \Livreur $livreur
     */
    public function setLivreur($livreur)
    {
        $this->livreur = $livreur;
    }



    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Commande
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \Utilisateur
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    /**
     * @param \Utilisateur $utilisateur
     */
    public function setUtilisateur($utilisateur)
    {
        $this->utilisateur = $utilisateur;
    }



    /**
     * Add ligne
     *
     * @param \VenteBundle\Entity\Lignecommande $ligne
     *
     * @return Commande
     */
    public function addLigne(\VenteBundle\Entity\Lignecommande $ligne)
    {
        $this->lignes[] = $ligne;

        return $this;
    }

    /**
     * Remove ligne
     *
     * @param \VenteBundle\Entity\Lignecommande $ligne
     */
    public function removeLigne(\VenteBundle\Entity\Lignecommande $ligne)
    {
        $this->lignes->removeElement($ligne);
    }

    /**
     * Get lignes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLignes()
    {
        return $this->lignes;
    }

    /**
     * @return string
     */
    public function getEtatLivraison()
    {
        return $this->etatLivraison;
    }

    /**
     * @param string $etatLivraison
     */
    public function setEtatLivraison($etatLivraison)
    {
        $this->etatLivraison = $etatLivraison;
    }




}

