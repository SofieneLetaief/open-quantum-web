<?php

namespace VenteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class LivreurType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('prenom')
            ->add('image', FileType::class, array('label' => 'Image(JPG)','data_class' => null))
            ->add('adresse',ChoiceType::class,array('choices'=>['Tunis'=>'Tunis','Ariana'=>'Ariana','Gafsa'=>'Gafsa','Nabeul'=>'Nabeul','Bizerte'=>'Bizerte','Beja'=>'Beja','Jendouba'=>'Jendouba',
                'Manubah'=>'Manubah','Ben Arous'=>'Ben Arous','Zaghouan'=>'Zaghouan','Siliana'=>'Siliana','Le Kef'=>'Le Kef','Sousse'=>'Sousse','Kairouan'=>'Kairouan',
                'Kasserine'=>'Kasserine','Monastir'=>'Monastir','Mahdia'=>'Mahdia','Sidi Bou Zid'=>'Sidi Bou Zid','Sfax'=>'Sfax','Gabes'=>'Gabes','Kebli'=>'Kebli',
                'Tozeur'=>'Tozeur','Medenine'=>'Medenine','Tataouine'=>'Tataouine'],'expanded'=>false,'multiple'=>false))
            ->add('numTel')
            ->add('active')
            ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VenteBundle\Entity\Livreur'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'ventebundle_livreur';
    }


}
