<?php

namespace MobileApiBundle\Controller;

use OpenQuantumBundle\Entity\Article;
use OpenQuantumBundle\Entity\Client;
use OpenQuantumBundle\Entity\Favoris;
use OpenQuantumBundle\Entity\FluxMsg;
use OpenQuantumBundle\Entity\Lignestock;
use OpenQuantumBundle\Entity\Livraison;
use OpenQuantumBundle\Entity\Reclamation;
use OpenQuantumBundle\Entity\Review;
use OpenQuantumBundle\Entity\Societe;
use OpenQuantumBundle\Entity\Utilisateur;
use OpenQuantumBundle\Services\ImageUpload;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use VenteBundle\Entity\Commande;
use VenteBundle\Entity\Lignecommande;
use VenteBundle\Entity\Livreur;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('MobileApiBundle:Default:index.html.twig');
    }

    //Url createUserJava

    public function createUtilisateurAction(Request $request)
    {
        $utilisateur = new utilisateur();
        $pwd = $request->get('plainPassword');

        $em = $this->getDoctrine()->getManager();
        $societe = $em->getRepository('OpenQuantumBundle:Societe')->find($request->get('idSociete'));

        $utilisateur->setIdsociete($societe);
        $utilisateur->setPlainPassword($pwd);
        $utilisateur->setNom($request->get('nom'));
        $utilisateur->setPrenom($request->get('prenom'));
        $utilisateur->setEmail($request->get('email'));
        $utilisateur->setUsername($request->get('username'));
        $utilisateur->setNumtel($request->get('numtel'));
        $utilisateur->setActive(true);
        $utilisateur->setEnabled(true);
        $em = $this->getDoctrine()->getManager();
        $em->persist($utilisateur);
        $em->flush();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($utilisateur);
        return new JsonResponse($formatted);
    }

    public function SendMsgAction(Request $request)
    {
        $flux = new FluxMsg();
        $flux->setIdSender($request->get('idsender'));
        $flux->setIdReciever($request->get('idreciever'));
        $flux->setContent($request->get('content'));
        $flux->setSeen(false);
        $em = $this->getDoctrine()->getManager();
        $em->persist($flux);
        $em->flush();

        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($flux);
        return new JsonResponse($formatted);
    }


    public function MsgSeenAction(Request $request)
    {
        //$flux = new FluxMsg();
        $idMsg= $request->get('idmsg');
        $em = $this->getDoctrine()->getManager();
        $msg = $em->getRepository('OpenQuantumBundle:FluxMsg')->find($idMsg);
        $msg->setSeen(true);
        $em->persist($msg);
        $em->flush();

        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($msg);
        return new JsonResponse($formatted);
    }


    public function GetMsgsAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $idSender= $request->get('idsender');
        $idReciever= $request->get('idreciever');

        $msgs = $em->getRepository('OpenQuantumBundle:FluxMsg')->findBy(['idSender'=>$idSender,'idReciever'=>$idReciever]);


        /*$res=array();
        foreach ($msgs as $msg) {

            $inMsg=new FluxMsg();


        $inMsg->setIdSender($msg->getIdSender());
        $inMsg->setIdReciever($msg->getIdReciever());
        $inMsg->setContent($msg->getContent());



            $serializer = new Serializer([new ObjectNormalizer()]);
            $formatted = $serializer->normalize($inMsg);


            $data = array(
                'idSender' => $msg->getIdSender(),
                'idReciever' => $msg->getIdReiever(),
                'content' => $msg->getContent(),

            );

            array_push($res,$data);
        }


*/

      /*  $serializer = new Serializer([new ObjectNormalizer()]);
        $response = new JsonResponse($res, 200);
        return $response;*/

        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($msgs);
        return new JsonResponse($formatted);
    }



    //Url loginUser/{username}/{password}
    public function loginMobileAction($username, $password)
    {
        $user_manager = $this->get('fos_user.user_manager');
        $factory = $this->get('security.encoder_factory');

        $data = [
            'type' => 'validation error',
            'title' => 'There was a validation error',
            'errors' => 'username or password invalide'
        ];
        $response = new JsonResponse($data, 400);

        $utilisateur = $user_manager->findUserByUsername($username);
        if (!$utilisateur)
            return $response;

        $encoder = $factory->getEncoder($utilisateur);

        $bool = ($encoder->isPasswordValid($utilisateur->getPassword(), $password, $utilisateur->getSalt())) ? "true" : "false";
        if ($bool == "true") {
            $role = $utilisateur->getRoles();
            //dump($role);
            $data = array('type' => 'Login succeed',
                'id' => $utilisateur->getId(),
                'nom' => $utilisateur->getNom(),
                'prenom' => $utilisateur->getPrenom(),
                'numTel' => $utilisateur->getNumtel(),
                'email' => $utilisateur->getEmail(),
                'role' => $role[0],
                'idSociete' => $utilisateur->getIdsociete()->getidSociete(),);

            $data2=array($data);


            $response = new JsonResponse($data2, 200);
            return $response;

        } else {
            return $response;

        }

    }

    //url listArticle
    public function getListArticleAction(Request $request)
    {
        $idSociete = $request->get('idSociete');
        $categorie = $request->get('categorie');
        if(is_null($categorie)){
            $categorie='All';
        }

        $em = $this->getDoctrine()->getManager();
        $all = $em->getRepository('OpenQuantumBundle:Article')->ListArticleFiltre($idSociete, $categorie);
        $articles = $all->getQuery()->execute();
        $articles = $em->getRepository('OpenQuantumBundle:Article')->JointureArticleStock($articles);
        foreach ($articles as $art) {
            $review = $em->getRepository('OpenQuantumBundle:Review')->findBy(['idarticle'=>$art->getIdarticle()]);
            $favoris=$em->getRepository('OpenQuantumBundle:Favoris')->findBy(['idarticle'=>$art->getIdarticle()]);
            $nbFavoris=count($favoris);
            $nbReview=count($review);
            $art->nbReview=$nbReview;
            $art->nbFavoris=$nbFavoris;
        }


        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($articles);
        return new JsonResponse($formatted);



    }
     //url listReview
    public function getListReviewAction(Request $request)
    {
        $idSociete=$request->get('idSociete');
        $em = $this->getDoctrine()->getManager();
        $review = $em->getRepository('OpenQuantumBundle:Review')->ListReview($idSociete);
        $serializer = new Serializer([new ObjectNormalizer()]);
        $res=array();
array_push($res,$review);
        $formatted = $serializer->normalize($review);
        return new JsonResponse($formatted);

    }

     //url listPromotion
    public function getListPromotionAction(Request $request)
    {
        $idSociete=$request->get('idsociete');
        $em = $this->getDoctrine()->getManager();
        $promotions = $em->getRepository('OpenQuantumBundle:Promotion')->findBy(['idsociete'=>$idSociete,'active'=>1]);
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($promotions);
        return new JsonResponse($formatted);
    }

    public function getListReclamationAction(Request $request)
    {
        //$idutilisateur=$request->get('idutilisateur');
        $em = $this->getDoctrine()->getManager();
        $Reclamation = $em->getRepository('OpenQuantumBundle:Reclamation')->findAll();
        $res=array();
        foreach ($Reclamation as $cat) {

            $Livraison=new Livraison();
            $Livraison->settrackingcode($cat->getIdlivraison()->gettrackingcode());

            //$Livraison->setdatecreation($cat->getIdlivraison()->getdatecreation());
            //$Livraison->setdatesortie($cat->getIdlivraison()->getdatesortie());
            $Livraison->setStatus($cat->getIdlivraison()->getStatus());
            $Livraison->setIdlivraison($cat->getIdlivraison()->getIdlivraison());
            $Article=new Article();
            $Article->setidarticle($cat->getidarticle()->getidarticle());
            $Article->setrefinterne($cat->getidarticle()->getrefinterne());
            $Article->setdesignation($cat->getidarticle()->getdesignation());


            $serializer = new Serializer([new ObjectNormalizer()]);
            $formatted = $serializer->normalize($Livraison);
            $formatted1 = $serializer->normalize($Article);

            $data = array(
                'id' => $cat->getIdrec(),
                'naturerec' => $cat->getNaturerec(),
                'Description' => $cat->getDescription(),
                'statut' => $cat->getStat(),
                'article' => $formatted1,
                'Livraison' => $formatted,
            );
            array_push($res,$data);
        }
        $test=array($res);

        $formatted = $serializer->normalize($res);
        $response = new JsonResponse($formatted, 200);
        return $response;
    }
    public function setReclamationArticleAction(Request $request)

        //$date=new \DateTime();

        /*$client  = $em->getRepository(Client::class)->find(1);
        $utilisateur  = $em->getRepository(Utilisateur::class)->find(1);

        /*$client = $em->getRepository('OpenQuantumBundle:Client')->findBy(['idclient'=>1]);*/










    {   $em = $this->getDoctrine()->getManager();
        $Article=$em->getRepository('OpenQuantumBundle:Article')->find($request->get('idarticle'));

        $idutilisateur=$request->get('idutilisateur');
        $idclient=$request->get('idclient');
        $idlivraison=$request->get('idlivraison');

        $utilisateur = $em->getRepository('OpenQuantumBundle:Utilisateur')->find($idutilisateur);


        $client = $em->getRepository('OpenQuantumBundle:Client')->findBy(['idclient'=>$idclient,'active'=>1]);
        //$livraison = $em->getRepository('OpenQuantumBundle:Livraison')->findBy(['idlivraison'=>$idlivraison,'active'=>1]);
        $livraison  = $em->getRepository(Livraison::class)->find(2);

        $client  = $em->getRepository(Client::class)->find(1);



        dump($livraison);

        $Reclamation=new Reclamation();
        $Reclamation->setNaturerec('article');
        $Reclamation->setDescription($request->get('description'));
        $Reclamation->setStat('En attente');
        $Reclamation->setIdarticle($Article);
        $Reclamation->setIdclient($client);
        $Reclamation->setIdLivraison($livraison);
        $Reclamation->setIdutilisateur($utilisateur);
        $img=$request->get('image');
//        $test=new ImageUpload("C:/xampp\htdocs\open-quantum-web\app/../web/Uploads/Images");
//
//       $upfile=new UploadedFile($img,$img);
//
//       $lastPath= $test->upload($upfile);
//    dump($lastPath);

        $Reclamation->setImage($img);
        $em->persist($Reclamation);
        $em->flush();

        return new JsonResponse("Reclamation added",200);

    }
    public function setReclamationlivraisonAction(Request $request)
    {   $em = $this->getDoctrine()->getManager();
        $Livraison=$em->getRepository('OpenQuantumBundle:Livraison')->find($request->get('idlivraison'));
//        $liv=new Livraison();
//         $liv->setTrackingcode($Livraison->getTrackingcode());
//         $liv->setIdlivraison($Livraison->getIdlivraison());



        $Reclamation=new Reclamation();
        $Reclamation->setNaturerec('livraison');
        $Reclamation->setDescription($request->get('description'));
        $Reclamation->setStat('En cour');
        $Reclamation->setIdLivraison($Livraison);
        $em->persist($Reclamation);
        $em->flush();
//        $serializer = new Serializer([new ObjectNormalizer()]);
//        $formatted = $serializer->normalize($Reclamation);
        return new JsonResponse("works",200);

    }

    //listCategorie
    public function getListCategorieAction(Request $request)
      {    $idSociete=$request->get('idSociete');
      $em = $this->getDoctrine()->getManager();
        $categorie = $em->getRepository('OpenQuantumBundle:Categorie')->ListCategorie($idSociete);
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($categorie);
        return new JsonResponse($formatted);

    }
    //listFamille
    public function getListFamilleAction(Request $request)
    {    $idSociete=$request->get('idSociete');
        $em = $this->getDoctrine()->getManager();
        $famille = $em->getRepository('OpenQuantumBundle:Famille')->findBy(['idsociete'=>$idSociete,'active'=>1]);
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($famille);
        return new JsonResponse($formatted);

    }


    public function getListCommandeUtilisateurAction(Request $request)
    {

        $idUtilisateur=$request->get('idUtilisateur');
        $em = $this->getDoctrine()->getManager();
        $commande = $em->getRepository('VenteBundle:Commande')->findBy(['utilisateur'=>$idUtilisateur,'active'=>1]);
        $res=array();
            foreach ($commande as $cat) {

            $liverur=new Livreur();
                $liverur->setImage($cat->getLivreur()->getImage());

                $liverur->setNom($cat->getLivreur()->getNom());
                $liverur->setPrenom($cat->getLivreur()->getPrenom());
                $liverur->setNumtel($cat->getLivreur()->getNumtel());
                $liverur->setAdresse($cat->getLivreur()->getAdresse());
                $liverur->setIdlivreur($cat->getLivreur()->getidLivreur());


                $serializer = new Serializer([new ObjectNormalizer()]);
                $formatted = $serializer->normalize($liverur);


            $data = array(
                'id' => $cat->getId(),
                'Date' => $cat->getDate(),
                'EtatLivraison' => $cat->getEtatLivraison(),
                'Livreur' => $formatted,
            );
                array_push($res,$data);
            }

            $response = new JsonResponse($res, 200);
            return $response;
    }
    public function setCommandeAction(Request $request)
    {
//        $commande=new Commande();
//        $query = http_build_query(array('aParam' => $data));
        $em = $this->getDoctrine()->getManager();

        $idUtilisateur=$request->get('idUtilisateur');
        $societeId=$request->get('idSociete');
        $livreurId=$request->get('idLivreur');

        $idArticles=$request->get('idArticles');
        $qtns=$request->get('qtn');
        $size=$request->get('size');



        $commande = new Commande();
        $commande->setDate(new \DateTime());
        $commande->setEtatLivraison('En attente');
        $user  = $em->getRepository(Utilisateur::class)->find($idUtilisateur);
        $societe  = $em->getRepository(Societe::class)->find($societeId);
        $livreur  = $em->getRepository(Livreur::class)->find(1);

        $commande->setLivreur($livreur);
        $commande->setUtilisateur($user);
        $commande->setActive(true);
        $commande->setSociete($societe);

        for ($i = 1; $i <= $size; $i++) {
            $idarticle = ($this->before(',', $idArticles));
            $qtnarticle = ($this->before(',', $qtns));

            $idArticles= substr($idArticles,strlen($idarticle)+1);
            $qtns = substr($qtns,strlen($qtnarticle)+1);

            $art = $em->getRepository(Article::class)->find($idarticle);

            $lignestock  = $em->getRepository(Lignestock::class)->findOneBy(['idarticle' => $idarticle] );


            $ligne = new LigneCommande();
            $ligne->setCommande($commande);
            $res=$lignestock->getQteStock()-$qtnarticle;
            $lignestock->setQteStock($res);
            $ligne->setActive(true);
            $ligne->setArticle($art);
            $ligne->setQuantite($qtnarticle);
            $em->persist($ligne);
            //$this->getDoctrine()->getManager()->flush();
            $commande->addLigne($ligne);

        }


        dump($commande);
        $em->persist($commande);
        $em->flush();

        return new JsonResponse("done");


    }

    ////        "$23-$1052"
    private function formatPrice($price, $qb)
    {
        $min = ($this->between('$', '-', $price));
        $price[0] = "-";
        $max = $this->after('$', $price);
        $qb->andWhere('a.prixvente <=:max')
            ->setParameter('max', $max);
        $qb->andWhere('a.prixvente >=:min')
            ->setParameter('min', $min);

    }

    private function between($t, $that, $inthat)
    {
        return $this->before($that, $this->after($t, $inthat));
    }

    function before($t, $inthat)
    {
        return substr($inthat, 0, strpos($inthat, $t));
    }

    function after($t, $inthat)
    {
        if (!is_bool(strpos($inthat, $t)))
            return substr($inthat, strpos($inthat, $t) + strlen($t));
    }

    public function setReviewAction(Request $request)
    {   $em = $this->getDoctrine()->getManager();
     $article=$em->getRepository('OpenQuantumBundle:Article')->find($request->get('idArticle'));
     $user=$em->getRepository('OpenQuantumBundle:Utilisateur')->find($request->get('idUtilisateur'));
        $date=new \DateTime();
        $review=new Review();
        $review->setDatereview($date);
        $review->setAddress($request->get('address'));
        $review->setIdutilisateur($user);
        $review->setDescription($request->get('description'));
        $review->setIdarticle($article);
        $review->setActive(1);
        $em->persist($review);
        $em->flush();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($review);
        return new JsonResponse($formatted,200);

    }

    public function editReviewAction(Request $request)
    {   $em = $this->getDoctrine()->getManager();
        $idReviewFromUrl=$request->get('idReview');
        $review=$em->getRepository('OpenQuantumBundle:Review')->find($idReviewFromUrl);
        $review->setDescription($request->get('description')    );
        $review->setDatereview($request->get('dateReview'));
        $em->flush();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($review);
        return new JsonResponse($formatted,200);
    }


    public function deleteReviewAction(Request $request)
    { $em = $this->getDoctrine()->getManager();
      $idReviewFromUrl=$request->get('idReview');
        $review=$em->getRepository('OpenQuantumBundle:Review')->find($idReviewFromUrl);
        $review->setActive(false);
        $em->flush();
        return new JsonResponse('works', 200);
    }

    public function getListSocieteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $societe = $em->getRepository('OpenQuantumBundle:Societe')->findAll();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($societe);
        return new JsonResponse($formatted);

    }

    //Getting users by societe id

    public function getUsersBySocieteIdAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $idSociete=$request->get('idsociete');
        $utilisateur = $em->getRepository('OpenQuantumBundle:Utilisateur')->findBy(['idsociete'=>$idSociete]);

        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($utilisateur);
        return new JsonResponse($formatted);



    }



    public function CountReviewAction(Request $request)
    {    $idArticle=$request->get('idArticle');
        $em = $this->getDoctrine()->getManager();
        $review = $em->getRepository('OpenQuantumBundle:Review')->findBy(['idarticle'=>$idArticle]);
        $nbLigne=count($review);
        $data = array(
            'nbReview' => $nbLigne,
        );
        $arr=array($data);
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($arr);
        return new JsonResponse($formatted);
    }




    //listLivraison
    public function getListLivraisonAction(Request $request)
    {
        $idSociete=$request->get('idSociete');
        $em = $this->getDoctrine()->getManager();
        $livraison = $em->getRepository('OpenQuantumBundle:Livraison')->findBy(['idsociete'=>$idSociete,'active'=>1]);
        $res=array();
        foreach ($livraison as $liv) {
            $liverur=new Livreur();
            $liverur->setNom($liv->getIdLivreur()->getNom());
            $liverur->setPrenom($liv->getIdLivreur()->getPrenom());
            $liverur->setNumtel($liv->getIdLivreur()->getNumtel());
            $liverur->setAdresse($liv->getIdLivreur()->getAdresse());
            $liverur->setIdlivreur($liv->getIdLivreur()->getidLivreur());
            $liverur->setImage($liv->getIdLivreur()->getImage());

            $serializer = new Serializer([new ObjectNormalizer()]);
            $formatted = $serializer->normalize($liverur);

            $data = array(
                'Id' => $liv->getIdLivraison(),
                'trackingcode' => $liv->getTrackingcode(),
                'Datecreation' => $liv->getDatecreation(),
                'DateArrival' => $liv->getDatesortie(),
                'Status' => $liv->getStatus(),
                'Livreur' => $formatted,
            );
            array_push($res,$data);
        }
        $data2=array($res);

        $response = new JsonResponse($res, 200);
        return $response;
    }

    public function getlistFavorisArticleAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $idUser=$request->get('idUtilisateur');
 $res= array();
        $Favoris=$em->getRepository('OpenQuantumBundle:Favoris')->findBy(['idutilisateur'=>$idUser]);
        foreach ($Favoris as $fav) {
            $data = array(
                'idArticleFav' => $fav->getIdarticle()->getIdArticle(),
            );



            array_push($res,$data);
        }
        $serializer = new Serializer([new ObjectNormalizer()]);

        $formatted = $serializer->normalize($res );
        return new JsonResponse($formatted,200);
    }


    public function setFavorisAction(Request $request)
    {   $em = $this->getDoctrine()->getManager();

        $user=$em->getRepository('OpenQuantumBundle:Utilisateur')->find($request->get('idUtilisateur'));
        $article=$em->getRepository('OpenQuantumBundle:Article')->find($request->get('idArticle'));
        $Favoris=new Favoris();
        $Favoris->setIdutilisateur($user);
        $Favoris->setIdarticle($article);
        $em->persist($Favoris);
        $em->flush();
        return new JsonResponse("workss",200);

    }
    public function deleteFavorisAction(Request $request)
    {   $em = $this->getDoctrine()->getManager();
        $idArticle=  $request->get('idArticle');
        $article=$em->getRepository('OpenQuantumBundle:Favoris')->findOneBy(['idarticle'=>$idArticle]);
        $em->remove($article);
        $em->flush();
        return new JsonResponse("workss",200);

    }


        public function countFavorisAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $idArticle=  $request->get('idArticle');
        $article=$em->getRepository('OpenQuantumBundle:Favoris')->findBy(['idarticle'=>$idArticle]);
        $nbLigne=count($article);

        $data = array(
            'nbFavoris' => $nbLigne,
        );

        $arr=array($data);
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($arr);
        return new JsonResponse($formatted);

    }
    public function deleteReclamationAction(Request $request)
    { $em = $this->getDoctrine()->getManager();
        $idRecFromUrl=$request->get('idRec');
        $reclamation=$em->getRepository('OpenQuantumBundle:Reclamation')->find($idRecFromUrl);
        $em->remove($reclamation);
        $em->flush();
        return new JsonResponse('works', 200);
    }



//    //url listLigneStock
//    public function getListLigneStockAction(Request $request)
//    {
//        $idSociete=$request->get('idSociete');
//        $em = $this->getDoctrine()->getManager();
//        $ligneStock = $em->getRepository('OpenQuantumBundle:Lignestock')->ListLigneStock($idSociete);
//        $serializer = new Serializer([new ObjectNormalizer()]);
//        $formatted = $serializer->normalize($ligneStock);
//        return new JsonResponse($formatted);
//    }


}
