<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;use Symfony\Component\HttpFoundation\Session\Session;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $session = new Session();
        $idSociete = $session->get('idSociete');

        $em = $this->getDoctrine()->getManager();

        $societes = $em->getRepository('OpenQuantumBundle:Societe')->findAll();

        $auth_checker = $this->get('security.authorization_checker');
        $router = $this->get('router');
        // 307: Internal Redirect
        if ($auth_checker->isGranted(['ROLE_ADMIN']) && $auth_checker->isGranted(['ROLE_USER'])) {
            // SUPER_ADMIN roles go to the `admin_home` route
            return new RedirectResponse($router->generate('open_quantum_homepage'), 307);
        }

        if ( $auth_checker->isGranted(['ROLE_USER'])) {
            // SUPER_ADMIN roles go to the `admin_home` route
            return new RedirectResponse($router->generate('ListArticle_index', [
                'Categorie' => 'All',
            ] ), 307);
        }


        /* if ($auth_checker->isGranted('ROLE_USER')) {
             // Everyone else goes to the `home` route
             return new RedirectResponse($router->generate(''), 307);
         }*/


        // replace this example code with whatever you need
        return $this->render('landingPage.html.twig', array(
            'societes' => $societes,
        ));
    }
}
