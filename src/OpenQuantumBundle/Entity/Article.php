<?php

namespace OpenQuantumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Article
 *
 * @ORM\Table(name="article", indexes={@ORM\Index(name="idUnite", columns={"idUnite"}), @ORM\Index(name="idModele", columns={"idModele"}), @ORM\Index(name="idTaxe", columns={"idTaxe"}), @ORM\Index(name="idCategorie", columns={"idCategorie"}), @ORM\Index(name="idSociete", columns={"idSociete"})})
 * @ORM\Entity(repositoryClass="OpenQuantumBundle\Repository\ArticleRepository")
 */
class Article
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idArticle", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idarticle;

    /**
     * @var string
     *
     * @ORM\Column(name="refInterne", type="string", length=50, nullable=false)
     * @Assert\NotBlank(message="Ajouter une image jpg")
     */
    private $refinterne;

    /**
     * @var string
     *
     * @ORM\Column(name="refFourn", type="string", length=50, nullable=false)
     * @Assert\NotBlank(message="Ajouter une image jpg")
     */
    private $reffourn;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=200, nullable=false)

     */
    private $designation;

    /**
     * @var string
     *
     * @ORM\Column(name="prixAchat", type="decimal", precision=5, scale=0, nullable=false)
     */
    private $prixachat;

    /**
     * @var string
     *
     * @ORM\Column(name="prixVente", type="decimal", precision=5, scale=0, nullable=false)
     */
    private $prixvente;

    /**
     * @var string
     * @ORM\Column(name="image", type="string", length=200, nullable=false)
     * @Assert\NotBlank(message="Ajouter une image jpg")
     * @Assert\File(mimeTypes={ "image/jpeg", "image/png" })
     */
    private $image;

    /**
     * @var integer
     * @ORM\Column(name="codeBarre", type="integer", nullable=true)
     */
    private $codebarre;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \Unite
     *
     * @ORM\ManyToOne(targetEntity="Unite")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idUnite", referencedColumnName="idUnite")
     * })
     */
    private $idunite;

    /**
     * @var \Modele
     *
     * @ORM\ManyToOne(targetEntity="Modele")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idModele", referencedColumnName="idModele")
     * })
     */
    private $idmodele;

    /**
     * @var \Taxe
     *
     * @ORM\ManyToOne(targetEntity="Taxe")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idTaxe", referencedColumnName="idTaxe")
     * })
     */
    private $idtaxe;

    /**
     * @var \Categorie
     *
     * @ORM\ManyToOne(targetEntity="Categorie")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idCategorie", referencedColumnName="idCategorie")
     * })
     */
    private $idcategorie;

    /**
     * @var \Societe
     *
     * @ORM\ManyToOne(targetEntity="Societe")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idSociete", referencedColumnName="idSociete")
     * })
     */
    private $idsociete;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=200, nullable=false)
     */
    private $description;

    public $qteStock;
    public $nbReview;
    public $nbFavoris;

    /**
     * @return int
     */
    public function getIdarticle()
    {
        return $this->idarticle;
    }

    /**
     * @param int $idarticle
     */
    public function setIdarticle($idarticle)
    {
        $this->idarticle = $idarticle;
    }

    /**
     * @return string
     */
    public function getRefinterne()
    {
        return $this->refinterne;
    }

    /**
     * @param string $refinterne
     */
    public function setRefinterne($refinterne)
    {
        $this->refinterne = $refinterne;
    }

    /**
     * @return string
     */
    public function getReffourn()
    {
        return $this->reffourn;
    }

    /**
     * @param string $reffourn
     */
    public function setReffourn($reffourn)
    {
        $this->reffourn = $reffourn;
    }

    /**
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * @param string $designation
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;
    }

    /**
     * @return string
     */
    public function getPrixachat()
    {
        return $this->prixachat;
    }

    /**
     * @param string $prixachat
     */
    public function setPrixachat($prixachat)
    {
        $this->prixachat = $prixachat;
    }

    /**
     * @return string
     */
    public function getPrixvente()
    {
        return $this->prixvente;
    }

    /**
     * @param string $prixvente
     */
    public function setPrixvente($prixvente)
    {
        $this->prixvente = $prixvente;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return int
     */
    public function getCodebarre()
    {
        return $this->codebarre;
    }

    /**
     * @param int $codebarre
     */
    public function setCodebarre($codebarre)
    {
        $this->codebarre = $codebarre;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return \Unite
     */
    public function getIdunite()
    {
        return $this->idunite;
    }

    /**
     * @param \Unite $idunite
     */
    public function setIdunite($idunite)
    {
        $this->idunite = $idunite;
    }

    /**
     * @return \Modele
     */
    public function getIdmodele()
    {
        return $this->idmodele;
    }

    /**
     * @param \Modele $idmodele
     */
    public function setIdmodele($idmodele)
    {
        $this->idmodele = $idmodele;
    }

    /**
     * @return \Taxe
     */
    public function getIdtaxe()
    {
        return $this->idtaxe;
    }

    /**
     * @param \Taxe $idtaxe
     */
    public function setIdtaxe($idtaxe)
    {
        $this->idtaxe = $idtaxe;
    }

    /**
     * @return \Categorie
     */
    public function getIdcategorie()
    {
        return $this->idcategorie;
    }

    /**
     * @param \Categorie $idcategorie
     */
    public function setIdcategorie($idcategorie)
    {
        $this->idcategorie = $idcategorie;
    }

    /**
     * @return \Societe
     */
    public function getIdsociete()
    {
        return $this->idsociete;
    }

    /**
     * @param \Societe $idsociete
     */
    public function setIdsociete($idsociete)
    {
        $this->idsociete = $idsociete;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function __get($prop)
    {
        return $this->$prop;
    }

    public function __isset($prop)
    {
        return isset($this->$prop);
    }


}

