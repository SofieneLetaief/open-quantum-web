<?php

namespace OpenQuantumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FluxMsg
 *
 * @ORM\Table(name="flux_msg")
 * @ORM\Entity(repositoryClass="OpenQuantumBundle\Repository\FluxMsgRepository")
 */
class FluxMsg
{

    /**
     * @var integer
     *
     * @ORM\Column(name="idMsg", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idMsg;


    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     *
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idSender", referencedColumnName="idUtilisateur")
     * })
     * @ORM\Column(name="idSender", type="integer")
     */
    private $idSender;

    /**
     * @var int
     *@ORM\ManyToOne(targetEntity="Utilisateur")
     *
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idReciever", referencedColumnName="idUtilisateur")
     * })
     * @ORM\Column(name="idReciever", type="integer")
     */
    private $idReciever;

    /**
     * @var string
     *
     * @ORM\Column(name="Content", type="string", length=255, nullable=true)
     */
    private $content;

    /**
     * @var boolean
     *
     * @ORM\Column(name="seen", type="boolean", nullable=false)
     */
    private $seen;

    /**
     * Set idSender
     *
     * @param integer $idSender
     *
     * @return FluxMsg
     */
    public function setIdSender($idSender)
    {
        $this->idSender = $idSender;

        return $this;
    }

    /**
     * Get idSender
     *
     * @return int
     */
    public function getIdSender()
    {
        return $this->idSender;
    }

    /**
     * @return bool
     */
    public function isSeen()
    {
        return $this->seen;
    }

    /**
     * @param bool $seen
     */
    public function setSeen($seen)
    {
        $this->seen = $seen;
    }

    /**
     * Set idReciever
     *
     * @param integer $idReciever
     *
     * @return FluxMsg
     */
    public function setIdReciever($idReciever)
    {
        $this->idReciever = $idReciever;

        return $this;
    }

    /**
     * Get idReciever
     *
     * @return int
     */
    public function getIdReciever()
    {
        return $this->idReciever;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return FluxMsg
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdMsg()
    {
        return $this->idMsg;
    }

    /**
     * @param int $idMsg
     */
    public function setIdMsg($idMsg)
    {
        $this->idMsg = $idMsg;
    }



    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
}

