<?php

namespace OpenQuantumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Abonnement
 *
 * @ORM\Table(name="abonnement")
 * @ORM\Entity
 */
class Abonnement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idAbonnement", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idabonnement;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=60, nullable=false)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=200, nullable=false)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbUtilisateur", type="integer", nullable=false)
     */
    private $nbutilisateur;

    /**
     * @var string
     *
     * @ORM\Column(name="prix", type="decimal", precision=5, scale=0, nullable=false)
     */
    private $prix;

    /**
     * @return int
     */
    public function getIdabonnement()
    {
        return $this->idabonnement;
    }

    /**
     * @param int $idabonnement
     */
    public function setIdabonnement($idabonnement)
    {
        $this->idabonnement = $idabonnement;
    }

    /**
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * @param string $titre
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getNbutilisateur()
    {
        return $this->nbutilisateur;
    }

    /**
     * @param int $nbutilisateur
     */
    public function setNbutilisateur($nbutilisateur)
    {
        $this->nbutilisateur = $nbutilisateur;
    }

    /**
     * @return string
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * @param string $prix
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;
    }


}

