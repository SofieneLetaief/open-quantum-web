<?php

namespace OpenQuantumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Modele
 *
 * @ORM\Table(name="modele", indexes={@ORM\Index(name="idConstructeur", columns={"idConstructeur"})})
 * @ORM\Entity
 */
class Modele
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idModele", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmodele;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=50, nullable=false)
     */
    private $libelle;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \Constructeur
     *
     * @ORM\ManyToOne(targetEntity="Constructeur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idConstructeur", referencedColumnName="idConstructeur")
     * })
     */
    private $idconstructeur;

    /**
     * @return int
     */
    public function getIdmodele()
    {
        return $this->idmodele;
    }

    /**
     * @param int $idmodele
     */
    public function setIdmodele($idmodele)
    {
        $this->idmodele = $idmodele;
    }

    /**
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param string $libelle
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return \Constructeur
     */
    public function getIdconstructeur()
    {
        return $this->idconstructeur;
    }

    /**
     * @param \Constructeur $idconstructeur
     */
    public function setIdconstructeur($idconstructeur)
    {
        $this->idconstructeur = $idconstructeur;
    }


}

