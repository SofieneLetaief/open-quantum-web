<?php

namespace OpenQuantumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Reclamation
 *
 * @ORM\Table(name="reclamation", indexes={@ORM\Index(name="idArticle", columns={"idArticle"}), @ORM\Index(name="idClient", columns={"idClient"})})
 * @ORM\Entity
 */
class Reclamation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idRec", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idrec;

    /**
     * @var string
     *
     * @ORM\Column(name="naturerec", type="string", length=50, nullable=false)
     */
    private $naturerec;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=50, nullable=false)
     */

    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="stat", type="string", length=100, nullable=false)
     */
    private $stat;

    /**
     * @var \Article
     *
     * @ORM\ManyToOne(targetEntity="Article")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idArticle", referencedColumnName="idArticle")
     * })
     */
    private $idarticle;

    /**
     * @var \Livraison
     *
     * @ORM\ManyToOne(targetEntity="Livraison")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idLivraison", referencedColumnName="idLivraison")
     * })
     */
    private $idLivraison;

    /**
     * @return string
     */


    /**
     * @var string
     * @ORM\Column(name="image", type="string", length=200, nullable=false)
     * @Assert\NotBlank(message="Ajouter une image jpg")
     * @Assert\File(mimeTypes={ "image/jpeg", "image/png" })
     */
    private $image;


    public function getStat()
    {
        return $this->stat;
    }

    /**
     * @param string $stat
     */
    public function setStat($stat)
    {
        $this->stat = $stat;
    }

    /**
     * @var \Client
     *
     * @ORM\ManyToOne(targetEntity="Client")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idClient", referencedColumnName="idClient")
     * })
     */
    private $idclient;

    /**
     * @return \Utilisateur
     */
    public function getIdutilisateur()
    {
        return $this->idutilisateur;
    }

    /**
     * @param \Utilisateur $idutilisateur
     */
    public function setIdutilisateur($idutilisateur)
    {
        $this->idutilisateur = $idutilisateur;
    }


    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idUtilisateur", referencedColumnName="idUtilisateur")
     * })
     */
    private $idutilisateur;

    /**
     * @return int
     */
    public function getIdrec()
    {
        return $this->idrec;
    }

    /**
     * @param int $idrec
     */
    public function setIdrec($idrec)
    {
        $this->idrec = $idrec;
    }

    /**
     * @return \Livraison
     */
    public function getIdLivraison()
    {
        return $this->idLivraison;
    }

    /**
     * @param \Livraison $idLivraison
     */
    public function setIdLivraison($idLivraison)
    {
        $this->idLivraison = $idLivraison;
    }

    /**
     * @return int
     */
    public function getNaturerec()
    {
        return $this->naturerec;
    }

    /**
     * @param int $naturerec
     */
    public function setNaturerec($naturerec)
    {
        $this->naturerec = $naturerec;
    }

    /**
     * @return int
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param int $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return \Article
     */
    public function getIdarticle()
    {
        return $this->idarticle;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param \Article $idarticle
     */
    public function setIdarticle($idarticle)
    {
        $this->idarticle = $idarticle;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return \Client
     */
    public function getIdclient()
    {
        return $this->idclient;
    }

    /**
     * @param \Client $idclient
     */
    public function setIdclient($idclient)
    {
        $this->idclient = $idclient;
    }


}

