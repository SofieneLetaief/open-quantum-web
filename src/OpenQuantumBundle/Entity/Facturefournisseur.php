<?php

namespace OpenQuantumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Facturefournisseur
 *
 * @ORM\Table(name="facturefournisseur", indexes={@ORM\Index(name="idFournisseur", columns={"idFournisseur"}), @ORM\Index(name="idModalite", columns={"idModalite"}), @ORM\Index(name="idArticle", columns={"idArticle"})})
 * @ORM\Entity
 */
class Facturefournisseur
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idfactureFournisseur", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idfacturefournisseur;

    /**
     * @var integer
     *
     * @ORM\Column(name="idFournisseur", type="integer", nullable=false)
     */
    private $idfournisseur;

    /**
     * @var integer
     *
     * @ORM\Column(name="idModalite", type="integer", nullable=false)
     */
    private $idmodalite;

    /**
     * @var integer
     *
     * @ORM\Column(name="idArticle", type="integer", nullable=false)
     */
    private $idarticle;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreation", type="datetime", nullable=false)
     */
    private $datecreation;

    /**
     * @var string
     *
     * @ORM\Column(name="remarque", type="text", length=65535, nullable=false)
     */
    private $remarque;

    /**
     * @var string
     *
     * @ORM\Column(name="etat", type="string", length=100, nullable=false)
     */
    private $etat;

    /**
     * @var string
     *
     * @ORM\Column(name="totalHT", type="decimal", precision=10, scale=0, nullable=false)
     */
    private $totalht;

    /**
     * @var string
     *
     * @ORM\Column(name="montantPaye", type="decimal", precision=10, scale=0, nullable=false)
     */
    private $montantpaye;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var string
     *
     * @ORM\Column(name="totalTTC", type="decimal", precision=10, scale=0, nullable=false)
     */
    private $totalttc;

    /**
     * @return int
     */
    public function getIdfacturefournisseur()
    {
        return $this->idfacturefournisseur;
    }

    /**
     * @param int $idfacturefournisseur
     */
    public function setIdfacturefournisseur($idfacturefournisseur)
    {
        $this->idfacturefournisseur = $idfacturefournisseur;
    }

    /**
     * @return int
     */
    public function getIdfournisseur()
    {
        return $this->idfournisseur;
    }

    /**
     * @param int $idfournisseur
     */
    public function setIdfournisseur($idfournisseur)
    {
        $this->idfournisseur = $idfournisseur;
    }

    /**
     * @return int
     */
    public function getIdmodalite()
    {
        return $this->idmodalite;
    }

    /**
     * @param int $idmodalite
     */
    public function setIdmodalite($idmodalite)
    {
        $this->idmodalite = $idmodalite;
    }

    /**
     * @return int
     */
    public function getIdarticle()
    {
        return $this->idarticle;
    }

    /**
     * @param int $idarticle
     */
    public function setIdarticle($idarticle)
    {
        $this->idarticle = $idarticle;
    }

    /**
     * @return \DateTime
     */
    public function getDatecreation()
    {
        return $this->datecreation;
    }

    /**
     * @param \DateTime $datecreation
     */
    public function setDatecreation($datecreation)
    {
        $this->datecreation = $datecreation;
    }

    /**
     * @return string
     */
    public function getRemarque()
    {
        return $this->remarque;
    }

    /**
     * @param string $remarque
     */
    public function setRemarque($remarque)
    {
        $this->remarque = $remarque;
    }

    /**
     * @return string
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @param string $etat
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;
    }

    /**
     * @return string
     */
    public function getTotalht()
    {
        return $this->totalht;
    }

    /**
     * @param string $totalht
     */
    public function setTotalht($totalht)
    {
        $this->totalht = $totalht;
    }

    /**
     * @return string
     */
    public function getMontantpaye()
    {
        return $this->montantpaye;
    }

    /**
     * @param string $montantpaye
     */
    public function setMontantpaye($montantpaye)
    {
        $this->montantpaye = $montantpaye;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return string
     */
    public function getTotalttc()
    {
        return $this->totalttc;
    }

    /**
     * @param string $totalttc
     */
    public function setTotalttc($totalttc)
    {
        $this->totalttc = $totalttc;
    }


}

