<?php

namespace OpenQuantumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contrat
 *
 * @ORM\Table(name="contrat", indexes={@ORM\Index(name="idSociete", columns={"idSociete"}), @ORM\Index(name="idAbonnement", columns={"idAbonnement"})})
 * @ORM\Entity
 */
class Contrat
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idContrat", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcontrat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDebut", type="datetime", nullable=false)
     */
    private $datedebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateFin", type="datetime", nullable=false)
     */
    private $datefin;

    /**
     * @var string
     *
     * @ORM\Column(name="etat", type="string", length=20, nullable=false)
     */
    private $etat;

    /**
     * @var \Societe
     *
     * @ORM\ManyToOne(targetEntity="Societe")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idSociete", referencedColumnName="idSociete")
     * })
     */
    private $idsociete;

    /**
     * @var \Abonnement
     *
     * @ORM\ManyToOne(targetEntity="Abonnement")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idAbonnement", referencedColumnName="idAbonnement")
     * })
     */
    private $idabonnement;

    /**
     * @return int
     */
    public function getIdcontrat()
    {
        return $this->idcontrat;
    }

    /**
     * @param int $idcontrat
     */
    public function setIdcontrat($idcontrat)
    {
        $this->idcontrat = $idcontrat;
    }

    /**
     * @return \DateTime
     */
    public function getDatedebut()
    {
        return $this->datedebut;
    }

    /**
     * @param \DateTime $datedebut
     */
    public function setDatedebut($datedebut)
    {
        $this->datedebut = $datedebut;
    }

    /**
     * @return \DateTime
     */
    public function getDatefin()
    {
        return $this->datefin;
    }

    /**
     * @param \DateTime $datefin
     */
    public function setDatefin($datefin)
    {
        $this->datefin = $datefin;
    }

    /**
     * @return string
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @param string $etat
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;
    }

    /**
     * @return \Societe
     */
    public function getIdsociete()
    {
        return $this->idsociete;
    }

    /**
     * @param \Societe $idsociete
     */
    public function setIdsociete($idsociete)
    {
        $this->idsociete = $idsociete;
    }

    /**
     * @return \Abonnement
     */
    public function getIdabonnement()
    {
        return $this->idabonnement;
    }

    /**
     * @param \Abonnement $idabonnement
     */
    public function setIdabonnement($idabonnement)
    {
        $this->idabonnement = $idabonnement;
    }


}

