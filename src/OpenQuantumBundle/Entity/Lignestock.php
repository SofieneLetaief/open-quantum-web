<?php

namespace OpenQuantumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Lignestock
 *
 * @ORM\Table(name="lignestock", indexes={@ORM\Index(name="idArticle", columns={"idArticle"}), @ORM\Index(name="idInventaire", columns={"idInventaire"})})
 * @ORM\Entity(repositoryClass="OpenQuantumBundle\Repository\LignestockRepository")
 */
class Lignestock
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idLigneStock", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idlignestock;

    /**
     * @var integer
     *
     * @ORM\Column(name="qteStock", type="integer", nullable=false)
     */
    private $qtestock;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \Article
     *
     * @ORM\ManyToOne(targetEntity="Article")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idArticle", referencedColumnName="idArticle")
     * })
     */
    private $idarticle;

    /**
     * @var \Inventaire
     *
     * @ORM\ManyToOne(targetEntity="Inventaire")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idInventaire", referencedColumnName="idInventaire")
     * })
     */
    private $idinventaire;

    /**
     * @return int
     */
    public function getIdlignestock()
    {
        return $this->idlignestock;
    }

    /**
     * @param int $idlignestock
     */
    public function setIdlignestock($idlignestock)
    {
        $this->idlignestock = $idlignestock;
    }

    /**
     * @return int
     */
    public function getQtestock()
    {
        return $this->qtestock;
    }

    /**
     * @param int $qtestock
     */
    public function setQtestock($qtestock)
    {
        $this->qtestock = $qtestock;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return \Article
     */
    public function getIdarticle()
    {
        return $this->idarticle;
    }

    /**
     * @param \Article $idarticle
     */
    public function setIdarticle($idarticle)
    {
        $this->idarticle = $idarticle;
    }

    /**
     * @return \Inventaire
     */
    public function getIdinventaire()
    {
        return $this->idinventaire;
    }

    /**
     * @param \Inventaire $idinventaire
     */
    public function setIdinventaire($idinventaire)
    {
        $this->idinventaire = $idinventaire;
    }


}

