<?php

namespace OpenQuantumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Promotion
 *
 * @ORM\Table(name="promotion", indexes={@ORM\Index(name="idSociete", columns={"idSociete"})})
 * @ORM\Entity
 */
class Promotion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idPourcentage", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idpourcentage;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=100, nullable=false)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=100, nullable=false)
     */
    private $nom;

    /**
     * @var integer
     *
     * @ORM\Column(name="usageMax", type="integer", nullable=false)
     */
    private $usagemax;

    /**
     * @var integer
     *
     * @ORM\Column(name="currentUsage", type="integer", nullable=false)
     */
    private $currentusage;

    /**
     * @var integer
     *
     * @ORM\Column(name="pourcentage", type="integer", nullable=false)
     */
    private $pourcentage;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDebut", type="datetime", nullable=false)
     */
    private $datedebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateExpiration", type="datetime", nullable=false)
     */
    private $dateexpiration;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \Societe
     *
     * @ORM\ManyToOne(targetEntity="Societe")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idSociete", referencedColumnName="idSociete")
     * })
     */
    private $idsociete;

    /**
     * @return int
     */
    public function getIdpourcentage()
    {
        return $this->idpourcentage;
    }

    /**
     * @param int $idpourcentage
     */
    public function setIdpourcentage($idpourcentage)
    {
        $this->idpourcentage = $idpourcentage;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return int
     */
    public function getUsagemax()
    {
        return $this->usagemax;
    }

    /**
     * @param int $usagemax
     */
    public function setUsagemax($usagemax)
    {
        $this->usagemax = $usagemax;
    }

    /**
     * @return int
     */
    public function getCurrentusage()
    {
        return $this->currentusage;
    }

    /**
     * @param int $currentusage
     */
    public function setCurrentusage($currentusage)
    {
        $this->currentusage = $currentusage;
    }

    /**
     * @return int
     */
    public function getPourcentage()
    {
        return $this->pourcentage;
    }

    /**
     * @param int $pourcentage
     */
    public function setPourcentage($pourcentage)
    {
        $this->pourcentage = $pourcentage;
    }

    /**
     * @return \DateTime
     */
    public function getDatedebut()
    {
        return $this->datedebut;
    }

    /**
     * @param \DateTime $datedebut
     */
    public function setDatedebut($datedebut)
    {
        $this->datedebut = $datedebut;
    }

    /**
     * @return \DateTime
     */
    public function getDateexpiration()
    {
        return $this->dateexpiration;
    }

    /**
     * @param \DateTime $dateexpiration
     */
    public function setDateexpiration($dateexpiration)
    {
        $this->dateexpiration = $dateexpiration;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return \Societe
     */
    public function getIdsociete()
    {
        return $this->idsociete;
    }

    /**
     * @param \Societe $idsociete
     */
    public function setIdsociete($idsociete)
    {
        $this->idsociete = $idsociete;
    }


}

