<?php

namespace OpenQuantumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Inventaire
 *
 * @ORM\Table(name="inventaire", indexes={@ORM\Index(name="idSociete", columns={"idSociete"})})
 * @ORM\Entity
 */
class Inventaire
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idInventaire", type="integer", nullable=false)
     * @ORM\Id
     */
    private $idinventaire;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateOuverture", type="datetime", nullable=false)
     */
    private $dateouverture;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateFermeture", type="datetime", nullable=false)
     */
    private $datefermeture;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \Societe
     *
     * @ORM\ManyToOne(targetEntity="Societe")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idSociete", referencedColumnName="idSociete")
     * })
     */
    private $idsociete;

    /**
     * @return int
     */
    public function getIdinventaire()
    {
        return $this->idinventaire;
    }

    /**
     * @param int $idinventaire
     */
    public function setIdinventaire($idinventaire)
    {
        $this->idinventaire = $idinventaire;
    }

    /**
     * @return \DateTime
     */
    public function getDateouverture()
    {
        return $this->dateouverture;
    }

    /**
     * @param \DateTime $dateouverture
     */
    public function setDateouverture($dateouverture)
    {
        $this->dateouverture = $dateouverture;
    }

    /**
     * @return \DateTime
     */
    public function getDatefermeture()
    {
        return $this->datefermeture;
    }

    /**
     * @param \DateTime $datefermeture
     */
    public function setDatefermeture($datefermeture)
    {
        $this->datefermeture = $datefermeture;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return \Societe
     */
    public function getIdsociete()
    {
        return $this->idsociete;
    }

    /**
     * @param \Societe $idsociete
     */
    public function setIdsociete($idsociete)
    {
        $this->idsociete = $idsociete;
    }


}

