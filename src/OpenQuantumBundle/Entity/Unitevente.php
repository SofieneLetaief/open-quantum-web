<?php

namespace OpenQuantumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Unitevente
 *
 * @ORM\Table(name="unitevente", indexes={@ORM\Index(name="idUnite", columns={"idUnite"}), @ORM\Index(name="idArticle", columns={"idArticle"})})
 * @ORM\Entity
 */
class Unitevente
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idUniteVente", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idunitevente;

    /**
     * @var string
     *
     * @ORM\Column(name="valeur", type="string", length=50, nullable=false)
     */
    private $valeur;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \Unite
     *
     * @ORM\ManyToOne(targetEntity="Unite")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idUnite", referencedColumnName="idUnite")
     * })
     */
    private $idunite;

    /**
     * @var \Article
     *
     * @ORM\ManyToOne(targetEntity="Article")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idArticle", referencedColumnName="idArticle")
     * })
     */
    private $idarticle;

    /**
     * @return int
     */
    public function getIdunitevente()
    {
        return $this->idunitevente;
    }

    /**
     * @param int $idunitevente
     */
    public function setIdunitevente($idunitevente)
    {
        $this->idunitevente = $idunitevente;
    }

    /**
     * @return string
     */
    public function getValeur()
    {
        return $this->valeur;
    }

    /**
     * @param string $valeur
     */
    public function setValeur($valeur)
    {
        $this->valeur = $valeur;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return \Unite
     */
    public function getIdunite()
    {
        return $this->idunite;
    }

    /**
     * @param \Unite $idunite
     */
    public function setIdunite($idunite)
    {
        $this->idunite = $idunite;
    }

    /**
     * @return \Article
     */
    public function getIdarticle()
    {
        return $this->idarticle;
    }

    /**
     * @param \Article $idarticle
     */
    public function setIdarticle($idarticle)
    {
        $this->idarticle = $idarticle;
    }


}

