<?php


namespace OpenQuantumBundle\Repository;


use Doctrine\ORM\EntityRepository;

class LignestockRepository extends EntityRepository
{

    function ListLigneStock($idSociete)
    {
        $query=$this->getEntityManager()
            ->createQuery('select r from OpenQuantumBundle:LigneStock r where r.idinventaire in (
                        select  DISTINCT(a.idinventaire)  
                        from OpenQuantumBundle:Inventaire a 
                        where  a.idsociete=:IdSociete ) 
                        and r.active=1 ')

            ->setParameter('IdSociete',$idSociete);

        return   $query->getResult()  ;
    }

}