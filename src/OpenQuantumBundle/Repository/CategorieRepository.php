<?php


namespace OpenQuantumBundle\Repository;


use Doctrine\ORM\EntityRepository;

class CategorieRepository extends EntityRepository
{

    function ListCategorie($idSociete)
    {
        $query=$this->getEntityManager()
            ->createQuery('select r from OpenQuantumBundle:Categorie r where r.idfamille in (
                        select  DISTINCT(a.idfamille)  
                        from OpenQuantumBundle:Famille a 
                        where  a.idsociete=:IdSociete ) 
                        and r.active=1 ')

            ->setParameter('IdSociete',$idSociete);

        return   $query->getResult()  ;
    }

}