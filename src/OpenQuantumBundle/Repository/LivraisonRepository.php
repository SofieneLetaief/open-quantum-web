<?php


namespace OpenQuantumBundle\Repository;


use Doctrine\ORM\EntityRepository;

class LivraisonRepository extends EntityRepository
{

    function customDQL($societeId)
    {
        $query = $this->getEntityManager()
            ->createQuery('select IDENTITY(f.idlivreur), count(f) as nbLivraison from OpenQuantumBundle:Livraison f where f.idsociete =:t 
            GROUP BY f.idlivreur')
            ->setParameter('t',$societeId);
        return $query->getResult();

    }




}