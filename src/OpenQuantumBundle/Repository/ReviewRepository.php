<?php


namespace OpenQuantumBundle\Repository;


use Doctrine\ORM\EntityRepository;

class ReviewRepository extends EntityRepository
{
    function ListReview($idSociete)
    {
    $query=$this->getEntityManager()
    ->createQuery('select r from OpenQuantumBundle:Review r where r.idarticle in (
                        select  DISTINCT(a.idarticle)  
                        from OpenQuantumBundle:Article a 
                        where  a.idsociete=:IdSociete ) 
                        and r.active=1 ')

    ->setParameter('IdSociete',$idSociete);

        return   $query->getResult()  ;
    }


}