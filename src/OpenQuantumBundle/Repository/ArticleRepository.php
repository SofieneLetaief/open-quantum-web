<?php


namespace OpenQuantumBundle\Repository;

use Doctrine\ORM\EntityRepository;


class ArticleRepository extends EntityRepository
{
    function ListPopulareArticle($idSociete)
    {
//    $query=$this->getEntityManager()
//    ->createQuery('select a from OpenQuantumBundle:Article a where a.idarticle in (
//                        select  count(lc.article) AS HIDDEN  nb  ,  lc.article  from VenteBundle:LigneCommande lc where lc.commande in
//                        ( select c.id from VenteBundle:Commande c where c.societe=:IdSociete)
//                         GROUP BY lc.article
//                         ORDER BY  nb  DESC )
//                                                                 ')
//    ->setParameter('IdSociete',$idSociete);
//        return   $query->getResult()  ;
//    }
//

        $conn = $this->getEntityManager()->getConnection();
        $sql = '  select idArticle  from lignecommande lc where idCommande in 
                       							   ( select (c.idCommande) from commande c where c.idsociete=:idsoc)
                        					  GROUP BY lc.idArticle
                        					  ORDER BY  SUM(quantite) DESC LIMIT 3 ';
        $stmt = $conn->prepare($sql);

        $stmt->execute(['idsoc' => $idSociete->getidSociete()]);

        $idArticles = $stmt->fetchAll();
        $popArtciles = array();

        foreach ($idArticles as $row) {
            array_push($popArtciles, $this->findOneBy(['idarticle' => $row['idArticle']]));

        }
        return ($popArtciles);
    }

    function ListArticleFiltre($idSociete,$Categorie)
    {

        $qb = $this->createQueryBuilder('qb')
            ->select('a')
            ->from('OpenQuantumBundle:Article','a')
            ->andWhere('a.idsociete =:idSoc')
            ->setParameter('idSoc', $idSociete);


        if ($Categorie != "All") {

            $oneCategorie = $this->getEntityManager()->getRepository('OpenQuantumBundle:Categorie')->findOneBy(['libelle' => $Categorie]);
            $qb->andWhere('a.idcategorie =:cate')
                ->setParameter('cate', $oneCategorie->getIdcategorie());
        }




               return $qb;


//           $query = $qb->getQuery();
//
//        return $query->execute();


    }

        function JointureArticleStock($articles)
        {
        foreach ($articles as $row) {
//            array_push($popArtciles,$this->findOneBy(['idarticle' => $row['idArticle']]));
            $qte = $this->getEntityManager()->getRepository('OpenQuantumBundle:LigneStock')->findOneBy(['idarticle' => $row->getidArticle()]);
            if (is_null($qte)) {
                $row->qteStock = 0;

            } else {

                $row->qteStock = $qte->getqteStock();

            }
        }
        return $articles;
    }





//        dump($qb->getQuery()->getResult());
//        return $qb->getQuery()->getResult();


//
//        if (!$includeUnavailableProducts) {
//            $qb->andWhere('p.available = TRUE')
//    }
//
//        $query = $qb->getQuery();
//
//        return $query->execute();





}

