<?php

namespace OpenQuantumBundle\Controller;

use OpenQuantumBundle\Entity\Inventaire;
use OpenQuantumBundle\Entity\Societe;
use OpenQuantumBundle\Entity\Utilisateur;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Societe controller.
 *
 */
class SocieteController extends Controller
{
    /**
     * Lists all societe entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $idSociete = $user->getIdSociete();
        $societes = $em->getRepository('OpenQuantumBundle:Societe')->findOneBy(['idsociete'=>$idSociete]);

        return $this->render('societe/index.html.twig', array(
            'societes' => $societes,
        ));
    }

    /**
     * Creates a new societe entity.
     *
     */
    public function newAction(Request $request)
    {
        $societe = new Societe();

        $formSociete = $this->createForm('OpenQuantumBundle\Form\SocieteType', $societe);
        $formSociete->handleRequest($request);


        $utilisateur = new Utilisateur();

        $formUtilisateur = $this->createForm('OpenQuantumBundle\Form\UtilisateurType', $utilisateur);
        $formUtilisateur->handleRequest($request);

        if ($formSociete->isSubmitted()) {
            dump($request);
            $em = $this->getDoctrine()->getManager();
            $societe->setActive(true);
            $em->persist($societe);
            $em->flush();

            $inventaire = new Inventaire();
            $inventaire->setIdsociete($societe);
            $inventaire->setActive(true);
            $inventaire->setIdinventaire($societe->getIdsociete());
            $date=new \DateTime();
            $date2=new \DateTime();
            date_add($date2,date_interval_create_from_date_string('1 year + 1 day'));
            $inventaire->setDateouverture($date);
            $inventaire->setDatefermeture($date2);
            $em->persist($inventaire);
            $em->flush();

        }


        if ($formUtilisateur->isSubmitted() && $formUtilisateur->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $ss = $em->getRepository(Societe::class)->find($societe->getIdsociete());
            $utilisateur->setIdsociete($ss);
            $utilisateur->addRole("ROLE_ADMIN");
            $utilisateur->setActive(true);
            $utilisateur->setEnabled(true);
            $em->persist($utilisateur);
            $em->flush();

      return $this->redirectToRoute('fos_user_security_login');
        }

        return $this->render('societe/new.html.twig', array(
            'societe' => $societe,
            'utilisateur' => $utilisateur,
            'formSociete' => $formSociete->createView(),
            'formUtilisateur' => $formUtilisateur->createView(),
        ));
    }

    /**
     * Finds and displays a societe entity.
     *
     */
    public function showAction(Societe $societe)
    {
        $deleteForm = $this->createDeleteForm($societe);

        return $this->render('societe/show.html.twig', array(
            'societe' => $societe,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing societe entity.
     *
     */
    public function editAction(Request $request, Societe $societe)
    {
        $deleteForm = $this->createDeleteForm($societe);
        $editForm = $this->createForm('OpenQuantumBundle\Form\SocieteType', $societe);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('societe_edit', array('idsociete' => $societe->getIdsociete()));
        }

        return $this->render('societe/edit.html.twig', array(
            'societe' => $societe,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a societe entity.
     *
     */
    public function deleteAction(Request $request, Societe $societe)
    {
        $form = $this->createDeleteForm($societe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $societe->setActive(false);
            $em->flush();
        }

        return $this->redirectToRoute('societe_index');
    }

    /**
     * Creates a form to delete a societe entity.
     *
     * @param Societe $societe The societe entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Societe $societe)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('societe_delete', array('idsociete' => $societe->getIdsociete())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Finds and displays a societe entity.
     *
     */
    public function societeDetailsAction()
    {
        $user =   $this->container->get('security.token_storage')->getToken()->getUser();
        $societeId = $user->getIdSociete();
        $em = $this->getDoctrine()->getManager();

        $currentSociete = $em->getRepository('OpenQuantumBundle:Societe')->find($societeId);
        $adresse = $currentSociete->getAdresse();
        dump($currentSociete);

        return $this->render('societe/profile.html.twig', array(
            'societeId' => $societeId,
            'currentSociete' => $currentSociete,
        ));
    }

}
