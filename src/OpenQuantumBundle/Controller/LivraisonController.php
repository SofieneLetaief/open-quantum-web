<?php

namespace OpenQuantumBundle\Controller;

use OpenQuantumBundle\Entity\Livraison;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\PieChart;
/**
 * Livraison controller.
 *
 */
class LivraisonController extends Controller
{
    /**
     * Lists all livraison entities.
     *
     */

    public function indexAction()
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $idSociete = $user->getIdSociete();
        $em = $this->getDoctrine()->getManager();

        $livraisons = $em->getRepository('OpenQuantumBundle:Livraison') ->findBy(['idsociete'=>$idSociete,'active'=>1]);

        $deliveredLivraison = 0;
        $undeliveredLivraison = 0;
        foreach($livraisons as $livraison)
        {
            if($livraison->getTrackingcode() == null){
                $undeliveredLivraison++;
            }else{
                $deliveredLivraison++;
            }
        }
       // dump($undeliveredLivraison);

        //New stuff here
        $result=$this->getDoctrine()->getRepository(Livraison::class)
            ->customDQL($idSociete);
        dump($result);
        $nbLines  =count($result);
        dump($nbLines);
        foreach($result as $key=>$val) {
            // Use $key as an index, or...

            // ... manage the index this way..
            dump($val);

        }
        //Livraison summary chart
        $pieChart = new PieChart();
        $pieChart->getData()->setArrayToDataTable(
            [['Task', 'Hours per Day'],
                ['Delivered',     $deliveredLivraison],
                ['Not Delivered',      $undeliveredLivraison]
            ]
        );
        $pieChart->getOptions()->setTitle('Livraison summary');
        $pieChart->getOptions()->setHeight(500);
        $pieChart->getOptions()->setWidth(900);
        $pieChart->getOptions()->getTitleTextStyle()->setBold(true);
        $pieChart->getOptions()->getTitleTextStyle()->setColor('#009900');
        $pieChart->getOptions()->getTitleTextStyle()->setItalic(true);
        $pieChart->getOptions()->getTitleTextStyle()->setFontName('Arial');
        $pieChart->getOptions()->getTitleTextStyle()->setFontSize(20);


        return $this->render('livraison/index.html.twig', array(
            'livraisons' => $livraisons,
            'societeId'=>$idSociete,
            'piechart' => $pieChart,
        ));
    }

    /**
     * Creates a new livraison entity.
     *
     */
    public function newAction(Request $request)
    {
        $user =   $this->container->get('security.token_storage')->getToken()->getUser();
        $societeId = $user->getIdSociete();

        $livraison = new Livraison();
        $form = $this->createForm('OpenQuantumBundle\Form\LivraisonType', $livraison);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $livraison->setIdsociete($societeId);
            $livraison->setActive(true);
            $em->persist($livraison);
            $em->flush();

            return $this->redirectToRoute('livraison_show', array('idlivraison' => $livraison->getIdlivraison()));
        }

        return $this->render('livraison/new.html.twig', array(
            'livraison' => $livraison,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a livraison entity.
     *
     */
    public function showAction(Livraison $livraison)
    {
        $deleteForm = $this->createDeleteForm($livraison);

        return $this->render('livraison/show.html.twig', array(
            'livraison' => $livraison,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing livraison entity.
     *
     */
    public function editAction(Request $request, Livraison $livraison)
    {
        $deleteForm = $this->createDeleteForm($livraison);
        $editForm = $this->createForm('OpenQuantumBundle\Form\LivraisonType', $livraison);
        $editForm->handleRequest($request);



        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $userNameLivraison = $livraison->getIdutilisateur()->getUsername();

            if($livraison->getTrackingcode() != NULL){

                //returns an instance of Vresh\TwilioBundle\Service\TwilioWrapper
                $twilio = $this->get('twilio.api');

                $message = $twilio->account->messages->sendMessage(
                '+17069141968', // From a Twilio number in your account
                '+21699377944', // Text any number
                " OpenQuantum Notification : " . "Bonjour " .$userNameLivraison. " votre votre commande est en cours de livraison "
                );

            }else{
                $twilio = $this->get('twilio.api');

                $message = $twilio->account->messages->sendMessage(
                    '+17069141968', // From a Twilio number in your account
                    '+21699377944', // Text any number
                    " OpenQuantum Notification : " . "Bonjour " .$userNameLivraison. " votre commande est prête à être expédiée  "
                );

            }
            return $this->redirectToRoute('livraison_edit', array('idlivraison' => $livraison->getIdlivraison()));
        }

        return $this->render('livraison/edit.html.twig', array(
            'livraison' => $livraison,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a livraison entity.
     *
     */
    public function deleteAction(Request $request, Livraison $livraison)
    {
        $form = $this->createDeleteForm($livraison);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $livraison->setActive(false);
            $em->flush();
        }

        return $this->redirectToRoute('livraison_index');
    }

    /**
     * Creates a form to delete a livraison entity.
     *
     * @param Livraison $livraison The livraison entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Livraison $livraison)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('livraison_delete', array('idlivraison' => $livraison->getIdlivraison())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }



}
