<?php

namespace OpenQuantumBundle\Controller;

use OpenQuantumBundle\Entity\Unite;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Unite controller.
 *
 */
class UniteController extends Controller
{
    /**
     * Lists all unite entities.
     *
     */
    public function indexAction()
    {

        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $idSociete = $user->getIdSociete();
        $unites = $em->getRepository('OpenQuantumBundle:Unite')->findBy(['idsociete'=>$idSociete,'active'=>1]);

        return $this->render('unite/index.html.twig', array(
            'unites' => $unites,
        ));
    }

    /**
     * Creates a new unite entity.
     *
     */
    public function newAction(Request $request)
    {
        $user =   $this->container->get('security.token_storage')->getToken()->getUser();
        $societeId = $user->getIdSociete();
        $unite = new Unite();
        $form = $this->createForm('OpenQuantumBundle\Form\UniteType', $unite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $unite->setIdsociete($societeId);
            $unite->setActive(true);
            $em->persist($unite);
            $em->flush();

            return $this->redirectToRoute('unite_show', array('idunite' => $unite->getIdunite()));
        }

        return $this->render('unite/new.html.twig', array(
            'unite' => $unite,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a unite entity.
     *
     */
    public function showAction(Unite $unite)
    {
        $deleteForm = $this->createDeleteForm($unite);

        return $this->render('unite/show.html.twig', array(
            'unite' => $unite,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing unite entity.
     *
     */
    public function editAction(Request $request, Unite $unite)
    {
        $deleteForm = $this->createDeleteForm($unite);
        $editForm = $this->createForm('OpenQuantumBundle\Form\UniteType', $unite);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('unite_index');
        }

        return $this->render('unite/edit.html.twig', array(
            'unite' => $unite,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a unite entity.
     *
     */
    public function deleteAction(Request $request, Unite $unite)
    {
        $form = $this->createDeleteForm($unite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $unite->setActive(false);
            $em->flush();
        }

        return $this->redirectToRoute('unite_index');
    }

    /**
     * Creates a form to delete a unite entity.
     *
     * @param Unite $unite The unite entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Unite $unite)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('unite_delete', array('idunite' => $unite->getIdunite())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
