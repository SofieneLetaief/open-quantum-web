<?php

namespace OpenQuantumBundle\Controller;

use OpenQuantumBundle\Entity\Modalite;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Modalite controller.
 *
 */
class ModaliteController extends Controller
{
    /**
     * Lists all modalite entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $idSociete = $user->getIdSociete();
        $modalites = $em->getRepository('OpenQuantumBundle:Modalite')->findBy(['idsociete'=>$idSociete,'active'=>1]);

        return $this->render('modalite/index.html.twig', array(
            'modalites' => $modalites,
        ));
    }

    /**
     * Creates a new modalite entity.
     *
     */
    public function newAction(Request $request)
    {
        $modalite = new Modalite();
        $form = $this->createForm('OpenQuantumBundle\Form\ModaliteType', $modalite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($modalite);
            $em->flush();

            return $this->redirectToRoute('modalite_show', array('idmodalite' => $modalite->getIdmodalite()));
        }

        return $this->render('modalite/new.html.twig', array(
            'modalite' => $modalite,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a modalite entity.
     *
     */
    public function showAction(Modalite $modalite)
    {
        $deleteForm = $this->createDeleteForm($modalite);

        return $this->render('modalite/show.html.twig', array(
            'modalite' => $modalite,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing modalite entity.
     *
     */
    public function editAction(Request $request, Modalite $modalite)
    {
        $deleteForm = $this->createDeleteForm($modalite);
        $editForm = $this->createForm('OpenQuantumBundle\Form\ModaliteType', $modalite);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('modalite_edit', array('idmodalite' => $modalite->getIdmodalite()));
        }

        return $this->render('modalite/edit.html.twig', array(
            'modalite' => $modalite,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a modalite entity.
     *
     */
    public function deleteAction(Request $request, Modalite $modalite)
    {
        $form = $this->createDeleteForm($modalite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $modalite->setActive(false);
            $em->flush();
        }

        return $this->redirectToRoute('modalite_index');
    }

    /**
     * Creates a form to delete a modalite entity.
     *
     * @param Modalite $modalite The modalite entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Modalite $modalite)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('modalite_delete', array('idmodalite' => $modalite->getIdmodalite())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
