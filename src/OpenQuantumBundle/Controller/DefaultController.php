<?php

namespace OpenQuantumBundle\Controller;

use CMEN\GoogleChartsBundle\GoogleCharts\Charts\PieChart;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $user =   $this->container->get('security.token_storage')->getToken()->getUser();
        dump($user);
        $societeId = $user->getIdSociete()->getIdSociete();

        $em = $this->getDoctrine()->getManager();
        $utilisateurs = $em->getRepository('OpenQuantumBundle:Utilisateur')->findBy(array('idsociete' => $societeId));
       //

        $em = $this->getDoctrine()->getManager();
        $idSociete = $user->getIdSociete();
        $fourn=     $em->getRepository('OpenQuantumBundle:Fournisseur')->findBy(['idsociete'=>$idSociete]);

        $achats = $em->getRepository('OpenQuantumBundle:Achat')->findBy(['idfournisseur'=>$fourn]);




        $commandes = $em->getRepository('VenteBundle:Commande')->findBy(array('societe' => $societeId));
        $nbCommande = count($commandes);
        $nbAchat = count($achats);
        $nbUser = count($utilisateurs);
        dump($nbUser);
        $livraisons = $em->getRepository('OpenQuantumBundle:Livraison') ->findBy(array('idsociete' => $societeId));
       $totalLivNb = count($livraisons);
        $deliveredLivraison = 0;
        $undeliveredLivraison = 0;
        foreach($livraisons as $livraison)
        {
            if($livraison->getTrackingcode() == null){
                $undeliveredLivraison++;
            }else{
                $deliveredLivraison++;
            }
        }

        //ALL STATS HERE
        //Livraison summary chart
        $pieChart = new PieChart();
        $pieChart->getData()->setArrayToDataTable(
            [['Task', 'Hours per Day'],
                ['Delivered',     $deliveredLivraison],
                ['Not Delivered',      $undeliveredLivraison]
            ]
        );
        $pieChart->getOptions()->setTitle('Livraison summary');
        $pieChart->getOptions()->setHeight(500);
        $pieChart->getOptions()->setWidth(800);
        $pieChart->getOptions()->getTitleTextStyle()->setBold(true);
        $pieChart->getOptions()->getTitleTextStyle()->setColor('#009900');
        $pieChart->getOptions()->getTitleTextStyle()->setItalic(true);
        $pieChart->getOptions()->getTitleTextStyle()->setFontName('Arial');
        $pieChart->getOptions()->getTitleTextStyle()->setFontSize(20);

        return $this->render('@OpenQuantum/Default/adminIndex.html.twig', array(
            'nbUsers' => $nbUser,
            'nbAchat'=> $nbAchat,
            'nbCommande' =>$nbCommande,
            'nbDeliveredP' =>$deliveredLivraison,
            'totalLiv' =>$totalLivNb,
            'livraionPie' => $pieChart,
        ));
    }
}
