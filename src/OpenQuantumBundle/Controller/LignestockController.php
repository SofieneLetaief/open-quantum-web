<?php

namespace OpenQuantumBundle\Controller;

use OpenQuantumBundle\Entity\Lignestock;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Lignestock controller.
 *
 */
class LignestockController extends Controller
{
    /**
     * Lists all lignestock entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $idSociete = $user->getIdSociete();
        $lignestocks = $em->getRepository('OpenQuantumBundle:Lignestock')->ListLigneStock($idSociete);



        return $this->render('lignestock/index.html.twig', array(
            'lignestocks' => $lignestocks,
        ));
    }

    /**
     * Creates a new lignestock entity.
     *
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $idSociete = $user->getIdSociete();
        $inv=  $em->getRepository('OpenQuantumBundle:Inventaire')->findOneBy(['idsociete'=>$idSociete]);

        $lignestock = new Lignestock();
        $form = $this->createForm('OpenQuantumBundle\Form\LignestockType', $lignestock);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $oldStock = $em->getRepository('OpenQuantumBundle:Lignestock')->findOneBy(['idarticle' =>$lignestock->getIdarticle(),'active'=>1 ]);


//            dump(empty($oldStock));
            if(!empty($oldStock)){
                 dump($oldStock->getQtestock());




                $lignestock->setQtestock($oldStock->getQtestock()+$lignestock->getQtestock());
                $em->remove($oldStock);
                $em->flush();


            }
            $lignestock->setIdinventaire($inv);
            $lignestock->setActive(true);
            $em->persist($lignestock);
            $em->flush();

            return $this->redirectToRoute('lignestock_show', array('idlignestock' => $lignestock->getIdlignestock()));
        }

        return $this->render('lignestock/new.html.twig', array(
            'lignestock' => $lignestock,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a lignestock entity.
     *
     */
    public function showAction(Lignestock $lignestock)
    {
        $deleteForm = $this->createDeleteForm($lignestock);

        return $this->render('lignestock/show.html.twig', array(
            'lignestock' => $lignestock,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing lignestock entity.
     *
     */
    public function editAction(Request $request, Lignestock $lignestock)
    {
        $deleteForm = $this->createDeleteForm($lignestock);
        $editForm = $this->createForm('OpenQuantumBundle\Form\LignestockType', $lignestock);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('lignestock_index');
        }

        return $this->render('lignestock/edit.html.twig', array(
            'lignestock' => $lignestock,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a lignestock entity.
     *
     */
    public function deleteAction(Request $request, Lignestock $lignestock)
    {
        $form = $this->createDeleteForm($lignestock);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $lignestock->setActive(false);
            $em->flush();
        }

        return $this->redirectToRoute('lignestock_index');
    }

    /**
     * Creates a form to delete a lignestock entity.
     *
     * @param Lignestock $lignestock The lignestock entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Lignestock $lignestock)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('lignestock_delete', array('idlignestock' => $lignestock->getIdlignestock())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
