<?php

namespace OpenQuantumBundle\Controller;

use OpenQuantumBundle\Entity\Fournisseur;
use OpenQuantumBundle\Entity\Utilisateur;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Fournisseur controller.
 *
 */
class FournisseurController extends Controller
{
    /**
     * Lists all fournisseur entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $idSociete = $user->getIdSociete();

        $fournisseurs = $em->getRepository('OpenQuantumBundle:Fournisseur')->findBy(['idsociete'=>$idSociete,'active'=>1]);

        return $this->render('fournisseur/index.html.twig', array(
            'fournisseurs' => $fournisseurs,
        ));
    }

    /**
     * Creates a new fournisseur entity.
     *
     */
    public function newAction(Request $request)
    {
        $fournisseur = new Fournisseur();
        $form = $this->createForm('OpenQuantumBundle\Form\FournisseurType', $fournisseur);
        $form->handleRequest($request);
        $x=$fournisseur->getRaisonsociale();
        $em = $this->getDoctrine()->getManager();
        //$tt = $em->getRepository('OpenQuantumBundle:Utilisateur')->findAll();
        $username = $this->getUser()->getUsername();
        if ($form->isSubmitted() && $form->isValid()) {
            $manager = $this->get('mgilet.notification');
            $notif = $manager->createNotification("Fournisseur ".$x. " ajouté par ".$username);
            $notif->setMessage('');

            $manager->addNotification(array($this->getUser()), $notif, true);


            $fournisseur->setActive(true);
            $em->persist($fournisseur);
            $em->flush();

            $aa=$fournisseur->getEmail();
            $message = \Swift_Message::newInstance()
                ->setSubject('OpenQuantum')
                ->setFrom('openquantumpidev@gmail.com')
                ->setTo($aa)
                ->setBody('Welcome to OpenQuantum '.$x);
            $this->get('mailer')->send($message);

            return $this->redirectToRoute('fournisseur_show', array('idfournisseur' => $fournisseur->getIdfournisseur()));
        }

        return $this->render('fournisseur/new.html.twig', array(
            'fournisseur' => $fournisseur,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a fournisseur entity.
     *
     */
    public function showAction(Fournisseur $fournisseur)
    {
        $deleteForm = $this->createDeleteForm($fournisseur);

        return $this->render('fournisseur/show.html.twig', array(
            'fournisseur' => $fournisseur,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing fournisseur entity.
     *
     */
    public function editAction(Request $request, Fournisseur $fournisseur)
    {
        $deleteForm = $this->createDeleteForm($fournisseur);
        $editForm = $this->createForm('OpenQuantumBundle\Form\FournisseurType', $fournisseur);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('fournisseur_index');
        }

        return $this->render('fournisseur/edit.html.twig', array(
            'fournisseur' => $fournisseur,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a fournisseur entity.
     *
     */
    public function deleteAction(Request $request, Fournisseur $fournisseur)
    {
        $form = $this->createDeleteForm($fournisseur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $fournisseur->setActive(false);
            $em->flush();
        }

        return $this->redirectToRoute('fournisseur_index');
    }

    /**
     * Creates a form to delete a fournisseur entity.
     *
     * @param Fournisseur $fournisseur The fournisseur entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Fournisseur $fournisseur)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('fournisseur_delete', array('idfournisseur' => $fournisseur->getIdfournisseur())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    public function imprimerAction() {
        $user=$this->container->get('security.token_storage')->getToken()->getUser();
        $idSociete = $user->getIdSociete();
        $em = $this->getDoctrine()->getManager();
        $news = $em->getRepository(Fournisseur::class)->findBy(['idsociete'=>$idSociete,'active'=>1]);


        return $this->render('fournisseur/print.html.twig',array('news'=>$news,'nom'=>$user));
    }

}
