<?php

namespace OpenQuantumBundle\Controller;

use OpenQuantumBundle\Entity\Constructeur;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Constructeur controller.
 *
 */
class ConstructeurController extends Controller
{
    /**
     * Lists all constructeur entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $idSociete = $user->getIdSociete();
        $constructeurs = $em->getRepository('OpenQuantumBundle:Constructeur')->findBy(['idsociete'=>$idSociete,'active'=>1]);

        return $this->render('constructeur/index.html.twig', array(
            'constructeurs' => $constructeurs,
        ));
    }

    /**
     * Creates a new constructeur entity.
     *
     */
    public function newAction(Request $request)

    {
        $user =   $this->container->get('security.token_storage')->getToken()->getUser();
        $societeId = $user->getIdSociete();
        $constructeur = new Constructeur();
        $form = $this->createForm('OpenQuantumBundle\Form\ConstructeurType', $constructeur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $constructeur->setIdsociete($societeId);
            $constructeur->setActive(true);
            $em->persist($constructeur);
            $em->flush();

            return $this->redirectToRoute('constructeur_show', array('idconstructeur' => $constructeur->getIdconstructeur()));
        }

        return $this->render('constructeur/new.html.twig', array(
            'constructeur' => $constructeur,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a constructeur entity.
     *
     */
    public function showAction(Constructeur $constructeur)
    {
        $deleteForm = $this->createDeleteForm($constructeur);

        return $this->render('constructeur/show.html.twig', array(
            'constructeur' => $constructeur,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing constructeur entity.
     *
     */
    public function editAction(Request $request, Constructeur $constructeur)
    {
        $deleteForm = $this->createDeleteForm($constructeur);
        $editForm = $this->createForm('OpenQuantumBundle\Form\ConstructeurType', $constructeur);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('constructeur_edit', array('idconstructeur' => $constructeur->getIdconstructeur()));
        }

        return $this->render('constructeur/edit.html.twig', array(
            'constructeur' => $constructeur,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a constructeur entity.
     *
     */
    public function deleteAction(Request $request, Constructeur $constructeur)
    {
        $form = $this->createDeleteForm($constructeur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
           $constructeur->setActive(false);
            $em->flush();
        }

        return $this->redirectToRoute('constructeur_index');
    }

    /**
     * Creates a form to delete a constructeur entity.
     *
     * @param Constructeur $constructeur The constructeur entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Constructeur $constructeur)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('constructeur_delete', array('idconstructeur' => $constructeur->getIdconstructeur())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
