<?php

namespace OpenQuantumBundle\Controller;

use OpenQuantumBundle\Entity\Taxe;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Taxe controller.
 *
 */
class TaxeController extends Controller
{
    /**
     * Lists all taxe entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $idSociete = $user->getIdSociete();

        $taxes = $em->getRepository('OpenQuantumBundle:Taxe')->findBy(['idsociete'=>$idSociete,'active'=>1]);

        return $this->render('taxe/index.html.twig', array(
            'taxes' => $taxes,
        ));
    }

    /**
     * Creates a new taxe entity.
     *
     */
    public function newAction(Request $request)
    {
        $user =   $this->container->get('security.token_storage')->getToken()->getUser();
        $societeId = $user->getIdSociete();
        $taxe = new Taxe();
        $form = $this->createForm('OpenQuantumBundle\Form\TaxeType', $taxe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $taxe->setIdsociete($societeId);
            $em->persist($taxe);
            $em->flush();

            return $this->redirectToRoute('taxe_show', array('idtaxe' => $taxe->getIdtaxe()));
        }

        return $this->render('taxe/new.html.twig', array(
            'taxe' => $taxe,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a taxe entity.
     *
     */
    public function showAction(Taxe $taxe)
    {
        $deleteForm = $this->createDeleteForm($taxe);

        return $this->render('taxe/show.html.twig', array(
            'taxe' => $taxe,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing taxe entity.
     *
     */
    public function editAction(Request $request, Taxe $taxe)
    {
        $deleteForm = $this->createDeleteForm($taxe);
        $editForm = $this->createForm('OpenQuantumBundle\Form\TaxeType', $taxe);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('taxe_edit', array('idtaxe' => $taxe->getIdtaxe()));
        }

        return $this->render('taxe/edit.html.twig', array(
            'taxe' => $taxe,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a taxe entity.
     *
     */
    public function deleteAction(Request $request, Taxe $taxe)
    {
        $form = $this->createDeleteForm($taxe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $taxe->setActive(false);
            $em->flush();
        }

        return $this->redirectToRoute('taxe_index');
    }

    /**
     * Creates a form to delete a taxe entity.
     *
     * @param Taxe $taxe The taxe entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Taxe $taxe)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('taxe_delete', array('idtaxe' => $taxe->getIdtaxe())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
