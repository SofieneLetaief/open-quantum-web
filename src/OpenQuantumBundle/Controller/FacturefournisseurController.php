<?php

namespace OpenQuantumBundle\Controller;

use OpenQuantumBundle\Entity\Facturefournisseur;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Facturefournisseur controller.
 *
 */
class FacturefournisseurController extends Controller
{
    /**
     * Lists all facturefournisseur entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $facturefournisseurs = $em->getRepository('OpenQuantumBundle:Facturefournisseur')->findBy(['active'=>1]);

        return $this->render('facturefournisseur/index.html.twig', array(
            'facturefournisseurs' => $facturefournisseurs,
        ));
    }

    /**
     * Creates a new facturefournisseur entity.
     *
     */
    public function newAction(Request $request)
    {
        $facturefournisseur = new Facturefournisseur();
        $form = $this->createForm('OpenQuantumBundle\Form\FacturefournisseurType', $facturefournisseur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($facturefournisseur);
            $em->flush();

            return $this->redirectToRoute('facturefournisseur_show', array('idfacturefournisseur' => $facturefournisseur->getIdfacturefournisseur()));
        }

        return $this->render('facturefournisseur/new.html.twig', array(
            'facturefournisseur' => $facturefournisseur,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a facturefournisseur entity.
     *
     */
    public function showAction(Facturefournisseur $facturefournisseur)
    {
        $deleteForm = $this->createDeleteForm($facturefournisseur);

        return $this->render('facturefournisseur/show.html.twig', array(
            'facturefournisseur' => $facturefournisseur,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing facturefournisseur entity.
     *
     */
    public function editAction(Request $request, Facturefournisseur $facturefournisseur)
    {
        $deleteForm = $this->createDeleteForm($facturefournisseur);
        $editForm = $this->createForm('OpenQuantumBundle\Form\FacturefournisseurType', $facturefournisseur);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('facturefournisseur_edit', array('idfacturefournisseur' => $facturefournisseur->getIdfacturefournisseur()));
        }

        return $this->render('facturefournisseur/edit.html.twig', array(
            'facturefournisseur' => $facturefournisseur,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a facturefournisseur entity.
     *
     */
    public function deleteAction(Request $request, Facturefournisseur $facturefournisseur)
    {
        $form = $this->createDeleteForm($facturefournisseur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $facturefournisseur->setActive(false);
            $em->flush();
        }

        return $this->redirectToRoute('facturefournisseur_index');
    }

    /**
     * Creates a form to delete a facturefournisseur entity.
     *
     * @param Facturefournisseur $facturefournisseur The facturefournisseur entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Facturefournisseur $facturefournisseur)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('facturefournisseur_delete', array('idfacturefournisseur' => $facturefournisseur->getIdfacturefournisseur())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
