<?php

namespace OpenQuantumBundle\Controller;

use OpenQuantumBundle\Entity\Reclamation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Reclamation controller.
 *
 * @Route("reclamation")
 */
class ReclamationController extends Controller
{
    /**
     * Lists all reclamation entities.
     *
     * @Route("/", name="reclamation_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $reclamations = $em->getRepository('OpenQuantumBundle:Reclamation')->findAll();

        return $this->render('reclamation/index.html.twig', array(
            'reclamations' => $reclamations,
        ));
    }

    /**
     * Creates a new reclamation entity.
     *
     * @Route("/new", name="reclamation_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $reclamation = new Reclamation();
        $form = $this->createForm('OpenQuantumBundle\Form\ReclamationType', $reclamation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user =   $this->container->get('security.token_storage')->getToken()->getUser();
            $reclamation->setIdutilisateur( $user);
            $reclamation->getNaturerec();
            $em->persist($reclamation);
            $em->flush();

            $aa=$user->getEmail();
            $bb=$user->getNom();
            $message = \Swift_Message::newInstance()
                ->setSubject('OpenQuantum Sav')
                ->setFrom('openquantumpidev@gmail.com')
                ->setTo($aa)
                ->setBody('Welcome to OpenQuantum '.$bb.'merci pour votre fedelite et nous esperons ' );
            $this->get('mailer')->send($message);

            return $this->redirectToRoute('reclamation_shows', array('idrec' => $reclamation->getIdrec()));
        }

        return $this->render('reclamation/new.html.twig', array(
            'reclamation' => $reclamation,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a reclamation entity.
     *
     * @Route("/{idrec}", name="reclamation_show")
     * @Method("GET")
     */
    public function showAction(Reclamation $reclamation)
    {
        $deleteForm = $this->createDeleteForm($reclamation);

        return $this->render('reclamation/show.html.twig', array(
            'reclamation' => $reclamation,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Finds and displays a reclamation entity.
     *
     * @Route("/test/{idrec}", name="reclamation_shows")
     * @Method("GET")
     */
    public function showclientAction(Reclamation $reclamation)
    {
        $deleteForm = $this->createDeleteForm($reclamation);

        return $this->render('reclamation/showclient.html.twig', array(
            'reclamation' => $reclamation,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing reclamation entity.
     *
     * @Route("/{idrec}/edit", name="reclamation_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Reclamation $reclamation)
    {
        $deleteForm = $this->createDeleteForm($reclamation);
        $editForm = $this->createForm('OpenQuantumBundle\Form\ReclamationType', $reclamation);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('reclamation_edit', array('idrec' => $reclamation->getIdrec()));
        }

        return $this->render('reclamation/edit.html.twig', array(
            'reclamation' => $reclamation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }


    /**
     * Displays a form to edit an existing reclamation entity.
     *
     * @Route("/{idrec}/Update", name="reclamation_Update")
     * @Method({"GET", "POST"})
     */
    public function editClientAction(Request $request, Reclamation $reclamation)
    {
        $deleteForm = $this->createDeleteForm($reclamation);
        $editForm = $this->createForm('OpenQuantumBundle\Form\ReclamationType', $reclamation);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('reclamation_index', array('idrec' => $reclamation->getIdrec()));
        }

        return $this->render('reclamation/Update.html.twig', array(
            'reclamation' => $reclamation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a reclamation entity.
     *
     * @Route("/{idrec}", name="reclamation_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Reclamation $reclamation)
    {
        $form = $this->createDeleteForm($reclamation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($reclamation);
            $em->flush();
        }

        return $this->redirectToRoute('reclamation_index');
    }

    /**
     * Creates a form to delete a reclamation entity.
     *
     * @param Reclamation $reclamation The reclamation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Reclamation $reclamation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('reclamation_delete', array('idrec' => $reclamation->getIdrec())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * @Route("/{idrec}/excell", name="reclamation_excel")
     */
    public function excelAction()
    {
        $em = $this->getDoctrine()->getManager();
        $reclamations = $em->getRepository('OpenQuantumBundle:Reclamation')->findAll();
        $writer = $this->container->get('egyg33k.csv.writer');
        $csv = $writer::createFromFileObject(new \SplTempFileObject());
        $csv->insertOne(['ID RECLAMATION', 'NATURE RECLAMATION', 'DESCRIPTION','stat']);
        foreach($reclamations as $reclamation){

            $csv->insertOne([$reclamation->getidrec(), $reclamation->getnaturerec(), $reclamation->getdescription(),$reclamation->getstat()]);
            $csv->insertOne([$reclamation->getidrec(),$reclamation->getIdarticle()->getDesignation()]);

        }


        $csv->output('users.csv');
        die('billehi i5dem aman ');
    }
}
