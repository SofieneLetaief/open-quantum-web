<?php

namespace OpenQuantumBundle\Controller;

use OpenQuantumBundle\Entity\Famille;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Famille controller.
 *
 */
class FamilleController extends Controller
{
    /**
     * Lists all famille entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $idSociete = $user->getIdSociete();
        $familles = $em->getRepository('OpenQuantumBundle:Famille')->findBy(['idsociete'=>$idSociete,'active'=>1]);

        return $this->render('famille/index.html.twig', array(
            'familles' => $familles,
        ));
    }

    /**
     * Creates a new famille entity.
     *
     */
    public function newAction(Request $request)
    {
        $famille = new Famille();
        $user =   $this->container->get('security.token_storage')->getToken()->getUser();
        $societeId = $user->getIdSociete();

        $form = $this->createForm('OpenQuantumBundle\Form\FamilleType', $famille);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $famille->setIdsociete($societeId);
            $em->persist($famille);
            $em->flush();

            return $this->redirectToRoute('famille_show', array('idfamille' => $famille->getIdfamille()));
        }

        return $this->render('famille/new.html.twig', array(
            'famille' => $famille,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a famille entity.
     *
     */
    public function showAction(Famille $famille)
    {
        $deleteForm = $this->createDeleteForm($famille);

        return $this->render('famille/show.html.twig', array(
            'famille' => $famille,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing famille entity.
     *
     */
    public function editAction(Request $request, Famille $famille)
    {
        $deleteForm = $this->createDeleteForm($famille);
        $editForm = $this->createForm('OpenQuantumBundle\Form\FamilleType', $famille);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('famille_edit', array('idfamille' => $famille->getIdfamille()));
        }

        return $this->render('famille/edit.html.twig', array(
            'famille' => $famille,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a famille entity.
     *
     */
    public function deleteAction(Request $request, Famille $famille)
    {
        $form = $this->createDeleteForm($famille);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $famille->setActive(false);
            $em->flush();
        }

        return $this->redirectToRoute('famille_index');
    }

    /**
     * Creates a form to delete a famille entity.
     *
     * @param Famille $famille The famille entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Famille $famille)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('famille_delete', array('idfamille' => $famille->getIdfamille())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
