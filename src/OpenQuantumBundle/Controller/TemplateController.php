<?php


namespace OpenQuantumBundle\Controller;


use http\Env\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class TemplateController extends Controller
{


    public function indexAction($Categorie, Request $request)
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $idSociete = $user->getIdSociete();
        $em = $this->getDoctrine()->getManager();
        $familles_categories = $em->getRepository('OpenQuantumBundle:Famille')->findBy(['idsociete' => $idSociete]);
        foreach ($familles_categories as $fam) {
            $categories = $em->getRepository('OpenQuantumBundle:Categorie')->findBy(['idfamille' => $fam->getidFamille()]);
            $fam->categories = $categories;
        }
        $pop = $em->getRepository('OpenQuantumBundle:Article')->ListPopulareArticle($idSociete);
        $all = $em->getRepository('OpenQuantumBundle:Article')->ListArticleFiltre($idSociete, $Categorie);
        $prices = array_column($all->getQuery()->execute(), 'prixvente');
        if (empty($prices)) {
            $max = 0;
            $min = 0;
        } else {
            $max = max($prices);
            $min = min($prices);
        }
        dump($request->query->get('SortBy'));
//       Alphabetically, A-Z
//      Alphabetically, Z-A
//        Price, low to high
//      Price, high to low
        if (!empty($request->query->get('SortBy'))) {
          $typeSort=  $request->query->get('SortBy');


            switch ($typeSort) {
                case 'Alphabetically, A-Z':
                    $all->orderBy('a.designation', 'ASC');
                    break;
                case 'Alphabetically, Z-A':
                    $all->orderBy('a.designation', 'DESC');
                    break;
                case 'Price, low to high':
                    $all->orderBy('a.prixvente', 'ASC');
                    break;

                case 'Price, high to low':
                    $all->orderBy('a.prixvente', 'DESC');
                    break;
            }

        }
        if (!empty($request->query->get('price'))) {
            $this->formatPrice($request->query->get('price'), $all);
        }
        dump($all->getQuery()->execute());
        $articles = $all->getQuery()->execute();
        $articles = $em->getRepository('OpenQuantumBundle:Article')->JointureArticleStock($articles);
        dump($articles);
        return $this->render('listArticle.html.twig', array(
            'popArticles' => $pop,
            'fam_cate' => $familles_categories,
            'articles' => $articles,
            'nbArticle' => count($articles),
            'max' => $max,
            'min' => $min,
            'cate'=>$Categorie,
        ));

    }

////        "$23-$1052"
    private function formatPrice($price, $qb)
    {
        $min = ($this->between('$', '-', $price));
        $price[0] = "-";
        $max = $this->after('$', $price);
        $qb->andWhere('a.prixvente <=:max')
            ->setParameter('max', $max);
        $qb->andWhere('a.prixvente >=:min')
            ->setParameter('min', $min);

    }

    private function between($t, $that, $inthat)
    {
        return $this->before($that, $this->after($t, $inthat));
    }

    function before($t, $inthat)
    {
        return substr($inthat, 0, strpos($inthat, $t));
    }

    function after($t, $inthat)
    {
        if (!is_bool(strpos($inthat, $t)))
            return substr($inthat, strpos($inthat, $t) + strlen($t));
    }
    public function ajoutCompareAction(SessionInterface $session,$idArticle)
    {
        $session->start();
        if ($session->get('ArticleCp') == null)
            $session->set('ArticleCp',[]);
        $ArticleCp = $session->get('ArticleCp');

        if(isset($ArticleCp[$idArticle])){

            $ArticleCp[$idArticle] = $idArticle;
        }
        else {
            $ArticleCp[$idArticle] = $idArticle;
        }

        $session->set('ArticleCp',$ArticleCp);
        return new \Symfony\Component\HttpFoundation\Response("Works ");
    }
    public function afficheCompareAction(SessionInterface $session)
    {
        $em = $this->getDoctrine()->getManager();
        $session->start();
        if ($session->get('ArticleCp') == null)
            $session->set('ArticleCp',[]);
        $ArticleCp = $session->get('ArticleCp');
        $articles=array();
        foreach ($ArticleCp as $p=>$item) {
            $articles[$p] = $em->getRepository('OpenQuantumBundle:Article')->find($item);
        }


        $articles = $em->getRepository('OpenQuantumBundle:Article')->JointureArticleStock($articles);

        return $this->render('compare.html.twig', array(
            'Articles' => $articles,
        ));

    }

    public function supprimerCompareAction(SessionInterface $session,$idArticle)
    {
        $session->start();
        $ArticleCp = $session->get('ArticleCp');
        unset($ArticleCp[$idArticle]);
        $session->set('ArticleCp',$ArticleCp);
        return new \Symfony\Component\HttpFoundation\Response("Works ");

    }






}