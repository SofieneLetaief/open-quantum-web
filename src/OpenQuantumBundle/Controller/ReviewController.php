<?php

namespace OpenQuantumBundle\Controller;

use OpenQuantumBundle\Entity\Review;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


/**
 * Review controller.
 *
 */
class ReviewController extends Controller
{
    /**
     * Lists all review entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $reviews = $em->getRepository('OpenQuantumBundle:Review')->findBy(['active'=>1]);
        $nbreview = 0;
        foreach ($reviews as $review){
            $nbreview ++;
        }

        $lastreview = 0;
        $date=new \DateTime();
        foreach ($reviews as $review){
            $daterev=$review->getDatereview();
            $interval = date_diff($daterev, $date);
            $datediff=$interval->days;
            if ($datediff < 30){
            $lastreview ++;
            }
        }

        $newreview = 0;
        $date=new \DateTime();
        foreach ($reviews as $review){
            $daterev=$review->getDatereview();
            $interval = date_diff($daterev, $date);
            $datediff=$interval->days;
            if ($datediff < 7){
                $newreview ++;
            }
        }

        dump($lastreview);
        dump($nbreview);
        return $this->render('review/index.html.twig', array(
            'reviews' => $reviews,
            'nbreviews' => $nbreview,
            'lastreview' => $lastreview,
            'newreview' => $newreview,

        ));
    }

    /**
     * Creates a new review entity.
     *
     */
    public function newAction(Request $request)
    {
        $review = new Review();
        $form = $this->createForm('OpenQuantumBundle\Form\ReviewType', $review);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($review);
            $em->flush();

            return $this->redirectToRoute('review_show', array('idreview' => $review->getIdreview()));
        }

        return $this->render('review/new.html.twig', array(
            'review' => $review,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a review entity.
     *
     */
    public function showAction(Review $review,Request $request)
    {

        $deleteForm = $this->createDeleteForm($review);
        return $this->render('review/show.html.twig', array(
            'review' => $review,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing review entity.
     *
     */
    public function editAction(Request $request, Review $review)
    {
        $deleteForm = $this->createDeleteForm($review);
        $editForm = $this->createForm('OpenQuantumBundle\Form\ReviewType', $review);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('review_index');
        }

        return $this->render('review/edit.html.twig', array(
            'review' => $review,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a review entity.
     *
     */
    public function deleteAction(Request $request, Review $review)
    {
        $form = $this->createDeleteForm($review);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $review->setActive(false);
            $em->flush();
        }

        return $this->redirectToRoute('review_index');
    }

    /**
     * Creates a form to delete a review entity.
     *
     * @param Review $review The review entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Review $review)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('review_delete', array('idreview' => $review->getIdreview())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }


    public function printAction (Review $review,Request $request){
        $deleteForm = $this->createDeleteForm($review);
        $snappy = $this->get('knp_snappy.pdf');

        $html = $this->renderView('review/test.html.twig', array(
            'review' => $review,
            'delete_form' => $deleteForm->createView(),
        ));

        $filename = 'myFirstSnappyPDF';

        return new Response(
            $snappy->getOutputFromHtml($html),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'inline; filename="'.$filename.'.pdf"'
            )
        );
    }

    public function addReviewAction (Request $request,$idArticle,$reviewMessage,$address){
        $em = $this->getDoctrine()->getManager();

        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        $article=$em->getRepository('OpenQuantumBundle:Article')->findOneBy(['idarticle'=>$idArticle]);
        $date=new \DateTime();
        $review = new Review();
        $review->setDescription($reviewMessage);
        $review->setAddress($address);
        $review->setActive(true);
        dump($article);


        $review->setIdarticle($article);
        $review->setIdutilisateur($user);
        $review->setDatereview($date);
        dump($review);
        $em->persist($review);
        $em->flush();

        return new \Symfony\Component\HttpFoundation\Response("Works ");




    }



}
