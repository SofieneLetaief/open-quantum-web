<?php

namespace OpenQuantumBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class SocieteType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('raisonsociale')
            ->add('matfiscale')
            ->add('numtel1')
            ->add('numtel2')
            ->add('email')
            ->add('adresse')
            ->add('ville')
            ->add('codepostal')
            ->add('pays')
            ->add('devise')
            ->add('logo', FileType::class, array('label' => 'Image(JPG)'))

            ->add('active', HiddenType::class, [
                'data' => 'true',
            ]);
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OpenQuantumBundle\Entity\Societe'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'openquantumbundle_societe';
    }


}
