<?php

namespace OpenQuantumBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LivraisonType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('trackingcode')
            ->add('datecreation', DateType::class, [

                'data' => new \DateTime(),
            ])
            ->add('datesortie', DateType::class, [

                'data' => new \DateTime(),
            ])
            ->add('status')
            ->add('active')
            ->add('idlivreur',EntityType::class,
                array(
                    'class'=>'VenteBundle:Livreur','choice_label'=>'nom'))
            ->add('idcommande',EntityType::class,
                array(
                    'class'=>'VenteBundle:Commande','choice_label'=>'id'))
            ->add('idutilisateur',EntityType::class,
                array(
                    'class'=>'OpenQuantumBundle:Utilisateur','choice_label'=>'username'))
            ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OpenQuantumBundle\Entity\Livraison'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'openquantumbundle_livraison';
    }


}
