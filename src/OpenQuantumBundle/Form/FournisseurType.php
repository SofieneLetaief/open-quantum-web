<?php

namespace OpenQuantumBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FournisseurType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('raisonsociale')
            ->add('numtel')
            ->add('email')
            ->add('adresse')
            ->add('matfiscale')
            ->add('contact')
            ->add('active')
            ->add('idsociete',EntityType::class,
                array(
                    'class'=>'OpenQuantumBundle:Societe','choice_label'=>'raisonsociale'))
        ->add('Captcha', 'Gregwar\CaptchaBundle\Type\CaptchaType',array(
        'width' => 200,
        'height' => 50,
        'length' => 6,
        'quality' => 90,
        'distortion' => true,
        'background_color' => [115, 194, 251],
        'max_front_lines' => 0,
        'max_behind_lines' => 0,
        'attr' => array('class' => 'form-control', 'rows'=> "6")));

    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OpenQuantumBundle\Entity\Fournisseur'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'openquantumbundle_fournisseur';
    }


}
