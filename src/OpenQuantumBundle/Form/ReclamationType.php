<?php

namespace OpenQuantumBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ReclamationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('naturerec',ChoiceType::class, [
            'choices' => ['technique' => 'technique', 'livraison' => 'livraison'],])
            ->add('description')
            ->add('image', FileType::class, array('label' => 'Image(JPG)','data_class' => null))
            ->add('stat',ChoiceType::class, [
                'choices' => ['en cour' => 'en cour', 'ok' => 'ok'],])
            ->add('idarticle',EntityType::class,
                array(
                    'class'=>'OpenQuantumBundle:Article','choice_label'=>'designation'))
            ->add('idlivraison',EntityType::class,
                array(
                    'class'=>'OpenQuantumBundle:livraison','choice_label'=>'trackingcode'))

            ->add('idclient',EntityType::class,
        array(
            'class'=>'OpenQuantumBundle:Client','choice_label'=>'raisonsociale'));
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OpenQuantumBundle\Entity\Reclamation'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }


}
