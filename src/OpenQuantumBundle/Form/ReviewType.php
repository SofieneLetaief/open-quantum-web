<?php

namespace OpenQuantumBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReviewType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('description')
            ->add('address')
            ->add('datereview')
            ->add('active')
            ->add('idarticle')
            ->add('idutilisateur')
        ->add('idarticle',EntityType::class,
        array(
            'class'=>'OpenQuantumBundle:Article','choice_label'=>'refInterne'))
        ->add('idutilisateur',EntityType::class,
        array(
            'class'=>'OpenQuantumBundle:Utilisateur','choice_label'=>'email'));
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OpenQuantumBundle\Entity\Review'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'openquantumbundle_review';
    }


}
