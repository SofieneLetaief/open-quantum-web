<?php

namespace OpenQuantumBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FacturefournisseurType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('idfournisseur')
            ->add('idmodalite',EntityType::class,
                array(
                    'class'=>'OpenQuantumBundle:Modalite','choice_label'=>'libelle'))
            ->add('idarticle',EntityType::class,
                array(
                    'class'=>'OpenQuantumBundle:Article','choice_label'=>'refInterne'))
           ->add('datecreation')
            ->add('remarque')
            ->add('etat')
            ->add('totalht')
            ->add('montantpaye')
            ->add('active')
            ->add('totalttc');
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OpenQuantumBundle\Entity\Facturefournisseur'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'openquantumbundle_facturefournisseur';
    }


}
