<?php

namespace OpenQuantumBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContratType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('datedebut')
            ->add('datefin')
            ->add('etat')
            ->add('idsociete',EntityType::class,
                array(
                    'class'=>'OpenQuantumBundle:Societe','choice_label'=>'raisonsociale'
                ))
            ->add('idabonnement',EntityType::class,
                array(
                    'class'=>'OpenQuantumBundle:Abonnement','choice_label'=>'titre'
                ));
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OpenQuantumBundle\Entity\Contrat'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'openquantumbundle_contrat';
    }


}
