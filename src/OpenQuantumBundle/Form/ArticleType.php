<?php

namespace OpenQuantumBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('refinterne')
            ->add('reffourn')
            ->add('designation')
            ->add('prixachat')
            ->add('prixvente')
            ->add('description')
            ->add('image', FileType::class, array('label' => 'Image(JPG)','data_class' => null))
            ->add('codebarre')
            ->add('active')
            ->add('idunite',EntityType::class,
                array(
                    'class'=>'OpenQuantumBundle:Unite','choice_label'=>'libelle'))
            ->add('idmodele',EntityType::class,
                array(
                    'class'=>'OpenQuantumBundle:Modele','choice_label'=>'libelle'))
            ->add('idtaxe',EntityType::class,
                array(
                    'class'=>'OpenQuantumBundle:Taxe','choice_label'=>'libelle'))
            ->add('idcategorie',EntityType::class,
                array(
                    'class'=>'OpenQuantumBundle:Categorie','choice_label'=>'libelle'))
           ;

    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OpenQuantumBundle\Entity\Article'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }


}
