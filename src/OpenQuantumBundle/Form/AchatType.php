<?php

namespace OpenQuantumBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AchatType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('quantite')
            ->add('datecreation')
            ->add('totalht')
            ->add('totalttc')
            ->add('remarque')
            ->add('etat')
            ->add('active')
            ->add('idfournisseur',EntityType::class,
                array(
                    'class'=>'OpenQuantumBundle:Fournisseur','choice_label'=>'raisonsociale'))

            ->add('idarticle',EntityType::class,
                array(
                    'class'=>'OpenQuantumBundle:Article','choice_label'=>'designation'));
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OpenQuantumBundle\Entity\Achat'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }


}
