<?php


namespace OpenQuantumBundle\EventListener;


use OpenQuantumBundle\Entity\Article;
use OpenQuantumBundle\Entity\Reclamation;
use OpenQuantumBundle\Entity\Societe;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

use OpenQuantumBundle\Services\ImageUpload;
use VenteBundle\Entity\Livreur;

class UploadImageListener
{

    private $uploader;

    public function __construct(ImageUpload $uploader)
    {
        $this->uploader = $uploader;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->uploadFile($entity);
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->uploadFile($entity);
    }

    private function uploadFile($entity)
    {
        // upload only works for Product entities
        if ($entity instanceof Societe) {
            $file = $entity->getLogo();

            // only upload new files
            if (!$file instanceof UploadedFile) {
                return;
            }

            $fileName = $this->uploader->upload($file);
            $entity->setLogo($fileName);

        }elseif ($entity instanceof Article){
            $file = $entity->getImage();

            // only upload new files
            if (!$file instanceof UploadedFile) {
                return;
            }

            $fileName = $this->uploader->upload($file);
            $entity->setImage($fileName);



        }elseif ($entity instanceof Livreur){
            $file = $entity->getImage();

            // only upload new files
            if (!$file instanceof UploadedFile) {
                return;
            }

            $fileName = $this->uploader->upload($file);
            $entity->setImage($fileName);



        }elseif ($entity instanceof Reclamation){
            $file = $entity->getImage();

            // only upload new files
            if (!$file instanceof UploadedFile) {
                return;
            }

            $fileName = $this->uploader->upload($file);
            $entity->setImage($fileName);



        }   else{

            return;
        }



    }

}